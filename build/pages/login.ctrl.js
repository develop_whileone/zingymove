/**
 * LOGIN PAGE CONTROLLER
 */

app.controller('loginPageCtrl', function($http, $location, $scope, $rootScope, $window) {
    console.log('loginPageCtrl called');
    $rootScope.hideLeftMenu = true;
    $rootScope.hideTopMenu = true;
    $rootScope.showFooter = false;
    $scope.errmsg = '';
    $scope.spinnerload='';
    $scope.alreadyExist = false;
    $scope.RememberMe = true;

    var getMfiLoginData = JSON.parse(localStorage.getItem("zingyMoveInfo"));
    if (getMfiLoginData == null) {
        window.location.href="#!/Login";
    } else {
        var userID = getMfiLoginData[0].UserID;
        var userType = getMfiLoginData[0].UserType;
        $rootScope.hideLeftMenu = false;
        $rootScope.hideTopMenu = false;
        window.location.href="#!/Dashboard";
        window.location.reload();
    }

    $scope.typeFiled = 'password';
    $scope.showPasswordValue = function(value){
        if(value == true){
            $scope.typeFiled = 'text';
        }
        else{
             $scope.typeFiled = 'password';
        }

    }

    $scope.onChangeUserName = function(value) {
        $scope.errmsg = '';
        if(value==''){
            $scope.errorUserName = "* Enter the User Name";
        } else {
            $scope.errorUserName = "";
        }
    }

    $scope.onChangePassword = function(value) {
        $scope.errmsg = '';
        if(value==''){
            $scope.errorPassword = "* Enter the Password";
        } else {
            $scope.errorPassword = "";
        }
    }

    $("#password").bind('keypress', function(e) {
        var code = (e.keyCode ? e.keyCode : e.which);
        if (code == 13) { 
            callLoginAPI();
        }
    });

    $scope.toggleRememberMe = function(event) {
        // alert(event.target.checked);
        $scope.RememberMe = event.target.checked;
    }

    $scope.btnSubmitLogin = function() {
        callLoginAPI();
    } // gotoLogin
    


    function callLoginAPI(){
        if($scope.username == '' && $scope.password == ''){
            $scope.errorUserName = "* Enter the User Name";
            $scope.errorPassword = "* Enter the Password";
            return false;
        } else if($scope.username == ''){
            $scope.errorUserName = "* Enter the User Name";
            return false;
        } else if($scope.password == ''){
            $scope.errorPassword = "* Enter the Password";
            return false;
        } else {
            $scope.spinnerload='fa fa-refresh fa-spin';
            $scope.alreadyExist = true;

            // var userTokenId = localStorage.getItem("UserToken");
            if($scope.username.length > 10) {
                var postdata = '{"userEmail":"'+$scope.username+'","userPassword":"'+$scope.password+'","userPhone":""}';
            } else {
                var postdata = '{"userPhone":"'+$scope.username+'","userPassword":"'+$scope.password+'","userEmail":""}';
            }
            
            console.log(postdata);
            var req = {
                method: 'POST',
                url: domainAddress+'LoginUser',
                data: postdata
            }
            $http(req).then(function(resp) {
                $scope.spinnerload='';
                $scope.alreadyExist = false;
                var response = resp.data;
                if(response.status === "success"){
                    // if($scope.RememberMe === true) {
                        localStorage.setItem("zingyMoveInfo", JSON.stringify(response.records));
                        // localStorage.setItem('ActiveMenu','Dashboard');
                    // }
                    var getZingyMoveLoginData = response.records;
                    // console.log(JSON.stringify(getZingyMoveLoginData));
                    console.log($scope.RememberMe);
                    $rootScope.$emit("UserAccessControlMethod", {});
                    if(getZingyMoveLoginData[0].userType == 'Owner'){
                        $rootScope.$emit("CallTopMenuMethod", {});
                        window.location.href="#!/Dashboard";
                    }
                    else if(getZingyMoveLoginData[0].userType == 'Renter'){
                        $rootScope.$emit("CallTopMenuMethod", {});
                        window.location.href="#!/Dashboard";
                    } else {
                        $rootScope.$emit("CallTopMenuMethod", {});
                        window.location.href="#!/Dashboard";
                    }
                    $scope.errmsg = '';
                } else {
                    $scope.errmsg = " * Please check the Username or Password";
                }
            });
        }
    }

});