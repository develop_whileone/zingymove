app.controller('topMenuController', function($location, $scope, $rootScope, $timeout, $sce) {
    topMenuMethod();
    $rootScope.$on("CallTopMenuMethod", function(){
        topMenuMethod();
    });

    function topMenuMethod() {
        var getZingyMoveUserData = JSON.parse(localStorage.getItem("zingyMoveUserInfo"));
        if(getZingyMoveUserData!=null){
            $scope.isLoggedIn = true;
            $scope.userType = getZingyMoveUserData[0].userType;
        } else {
            $scope.isLoggedIn = false;
        }
    } // topMenuMethod()
    
    $scope.logOut = function() {
        localStorage.clear();
        window.location.href="#!/Landing";
        window.location.reload();
    };
});