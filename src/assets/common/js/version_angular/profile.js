app.controller('profileController', function($timeout, $http, $rootScope, $window, $scope, $sce) {

    var getZingyMoveUserData = JSON.parse(localStorage.getItem("zingyMoveUserInfo"));
    if(getZingyMoveUserData!=null){
        $scope.userID = getZingyMoveUserData[0].userID;
        $scope.userType = getZingyMoveUserData[0].userType;
        if($scope.userType != 'Super Admin') {
            $scope.userMail = getZingyMoveUserData[0].userEmail;
            $scope.userPhone = getZingyMoveUserData[0].userPhone;
            $scope.usertitle = getZingyMoveUserData[0].userTitle;
            $scope.username = getZingyMoveUserData[0].userName;
            $scope.password = getZingyMoveUserData[0].userPassword;
            $scope.confirmPassword = getZingyMoveUserData[0].userPassword;
        }
    }

    $scope.onUserTypeChange = function(userType) {
        if(userType != '' && userType != null && userType != undefined) {
            $scope.errorUserType = false;
        } else {
            $scope.errorUserType = true;            
        }

        $scope.adduserType = userType;
    }
    $scope.onUserTitleChange = function(usertitle) {
        if(usertitle != '' && usertitle != null && usertitle != undefined) {
            $scope.errorUsertitle = false;
        } else {
            $scope.errorUsertitle = true;            
        }
    }

    $scope.onUserNameChange = function(userName) {
        if(userName != '' && userName != null && userName != undefined) {
            $scope.errorUserName = false;
        } else {
            $scope.errorUserName = true;            
        }
    }

    $scope.onUserMailChange = function(userMail) {
        if ($scope.userForm.userMail.$valid) {
            $scope.errorUserMail = false;
        } else {
            $scope.errorUserMail = true;
        }
    }

    $scope.onUserPhoneChange = function(userPhone) {
        if(userPhone == '' || userPhone == undefined) {
            $scope.errorUserPhone = true;
        } else if(isNaN(userPhone)) {
            $scope.errorUserPhone = true;
        } else {
            $scope.errorUserPhone = false;            
        }
    }

    $scope.onPasswordChange = function(password) {
        if(password != '' && password != null && password != undefined) {
            $scope.errorPassword = false;
        } else {
            $scope.errorPassword = true;            
        }
    }

    $scope.onConfirmPasswordChange = function(confirmPassword) {
        if(confirmPassword == '' || confirmPassword == undefined) {
            $scope.errorConfirmPassword = true;
            $scope.errorPasswordNotMatch = false;
        } else if(confirmPassword != $scope.password) {
            $scope.errorConfirmPassword = false;
            $scope.errorPasswordNotMatch = true;
        } else {
            $scope.errorConfirmPassword = false;
            $scope.errorPasswordNotMatch = false;
        }
    }

    $scope.AddUser = function() {

        // else if($scope.confirmPassword == '' || $scope.confirmPassword == undefined) {
        //     $scope.errorConfirmPassword = true;
        // } else if($scope.confirmPassword != $scope.password) {
        //     $scope.errorPasswordNotMatch = true;
        // } else if($scope.password == '' || $scope.password == undefined){
        //     $scope.errorPassword = true;
        // } 

        if($scope.adduserType == '' || $scope.adduserType == undefined) {
            $scope.errorUserType = true;
        } else if($scope.usertitle == '' || $scope.usertitle == undefined) {
            $scope.errorUsertitle = true;
        } else if($scope.username == '' || $scope.username == undefined) {
            $scope.errorUserName = true;
        } else if($scope.userMail == '' || $scope.userMail == undefined) {
            $scope.errorUserMail = true;
        } else if($scope.userPhone == '' || $scope.userPhone == undefined) {
            $scope.errorUserPhone = true;
        } else if(isNaN($scope.userPhone)) {
            $scope.errorUserPhone = true;
        } else { 
            var postdata = '{"userTitle":"'+$scope.usertitle+'","userName":"'+$scope.username+'","userEmail": "'+$scope.userMail+'","userPhone": "'+$scope.userPhone+'","userPassword": "Password#1","userType": "'+$scope.adduserType+'","adminID":"'+$scope.userID+'"}';
            var req = {
                method: 'POST',
                url: domainAddress+'CreateUser',
                data: postdata
            }
            $http(req).then(function(resp) {
                var response = resp.data;
                if(response.status === "Success"){
                    swal("Success!", "User Added Successfully!", "success");
                    $('#userForm')[0].reset();
                    $("html, body").animate({ scrollTop: 0 }, "slow");
                    $scope.usertitle = '';
                    $scope.username = '';
                    $scope.userMail = '';
                    $scope.userPhone = '';
                    $scope.password = '';
                    $scope.userType = '';                    
                } else if(response.status === 'Exists') {
                    swal("Oops!", "User Already Exists!", "warning");
                } else {
                    swal("Oops!", "Something went wrong!", "error");
                }
            });
        }
    }
});