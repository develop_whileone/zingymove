app.controller('renterDashboardController', function($timeout, $http, $rootScope, $window, $scope, $sce, $compile) {
    $scope.noProperty = false;
    $scope.showFavourites = false;
    var getZingyMoveUserData = JSON.parse(localStorage.getItem("zingyMoveUserInfo"));
    if(getZingyMoveUserData!=null){
        $scope.userID = getZingyMoveUserData[0].userID;
        $scope.userType = getZingyMoveUserData[0].userType;
        $scope.userEmail = getZingyMoveUserData[0].userEmail;
        $scope.userPhone = getZingyMoveUserData[0].userPhone;
        
        if($scope.userType == 'Renter') {
            $scope.showContactBtn = true;
        }
    }
    var req = {
        method: 'GET',
        url: domainAddress+'GetAllPropertyList',
    }
    $http(req).then(function(resp) {
        
        var response = resp.data;
        if(response.status === "success"){
            var records = response.records;
            for(var i = 0; i < records.length; i++) {
                if(records[i].propertyImages != null && records[i].propertyImages != undefined) {
                    for(var j = 0; j < records[i].propertyImages.length; j++) {
                        records[i].propertyImages[j] = domainAddress+records[i].propertyImages[j];
                    }
                }
            }
            $scope.resultAllProperties = records;
            
            var x = 0;
            for(var i = 0; i < records.length; i++) {
                if(records[i].appointments != undefined) {
                    var appointments = records[i].appointments;
                    for(var j = 0; j < appointments.length; j++) {
                        if($scope.userID == appointments[j].appointmentFrom) {
                            
                            $scope.appointment = appointments[j];
                            console.log(appointments[j].appointmentStatus);
                            // if(appointments[j].appointmentStatus != 'Visited' && appointments[j].appointmentStatus != 'Rated') {
                                $scope.resultAllProperties[i].appointmentUser = appointments[j];
                            // }
                        }
                    }
                }
            }
            $scope.noProperty = false;
        } else {
            console.log("Failed");
            $scope.noProperty = true;
        }
    });
    $scope.goToDetailsPage = function(propertyID) {
        window.location.href="#!/PropertyDetails?propID="+propertyID;
        window.location.reload();
    }

    $scope.searchProperties = function() {
        var minPrice = $("#priceMinValue").val().replace(/\D/g,'');
        var maxPrice = $("#priceMaxValue").val().replace(/\D/g,'');

        var propertyCity = $scope.propertyCity;
        var propertyType = $scope.propertyType;

        // console.log(minPrice+" - "+maxPrice+" - "+propertyCity+" - "+propertyType);
        
        var postData = '{ "minPrice":"'+minPrice+'","maxPrice":"'+maxPrice+'","propertyCity":"'+propertyCity+'","propertyType":"'+propertyType+'" }';
        console.log(postData);
        var req = {
            method: 'POST',
            url: domainAddress+'SearchProperties',
            data: postData
        }
        $http(req).then(function(resp) {
            if(resp.data.status === "success"){
                var records = resp.data.records;
                for(var i = 0; i < records.length; i++) {
                    if(records[i].propertyImages != null && records[i].propertyImages != undefined) {
                        for(var j = 0; j < records[i].propertyImages.length; j++) {
                            records[i].propertyImages[j] = domainAddress+records[i].propertyImages[j];
                        }
                    }
                }
                $scope.resultAllProperties = records;

                console.log(records);
            } else if(resp.data.status === "failure"){
                if(resp.data.record_count == 0) {
                    console.log("No Properties found");
                }
            } else {
                console.log("Error in  Search");
            }
        });
    }

    $scope.openAppointmentDetails = function(prop) {
        $scope.propDetails = prop;
        $scope.propAppointments = prop.appointmentUser;
        // console.log($scope.propAppointments);
        $scope.appointment = prop.appointmentUser;
        $("#modal-appointments").modal("toggle");
    }

    $scope.openScheduleModel = function(appointment) {
        $("#modal-appointments").modal("toggle");
        setTimeout(() => {
            $("#rescheduleModal").modal("toggle");
        },300);
    }


    $scope.showAvailableHours = false;
    
    var weekday = new Array("Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday");
    $("#contactDate").on('dp.change', function(e) {
        var day = weekday[e.date._d.getDay()];
        var propDetails = $scope.propDetails;
        if(day == 'Sunday') {
            $scope.availableHoursArr = [];
            if(propDetails.sundayAMFrom != undefined && propDetails.sundayAMTill != undefined) {
                $scope.availableHoursArr.push(propDetails.sundayAMFrom+" - "+propDetails.sundayAMTill);
            } else if(propDetails.sundayAMFrom != undefined) {
                $scope.availableHoursArr.push(propDetails.sundayAMFrom);
            } else if(propDetails.sundayAMTill != undefined) {
                $scope.availableHoursArr.push(propDetails.sundayAMTill);
            }

            if(propDetails.sundayPMFrom != undefined && propDetails.sundayPMTill != undefined) {
                $scope.availableHoursArr.push(propDetails.sundayPMFrom+" - "+propDetails.sundayPMTill);
            } else if(propDetails.sundayPMFrom != undefined) {
                $scope.availableHoursArr.push(propDetails.sundayPMFrom);
            } else if(propDetails.sundayPMTill != undefined) {
                $scope.availableHoursArr.push(propDetails.sundayPMTill);
            }
        } else if(day == 'Monday') {
            $scope.availableHoursArr = [];
            if(propDetails.mondayAMFrom != undefined && propDetails.mondayAMTill != undefined) {
                $scope.availableHoursArr.push(propDetails.mondayAMFrom+" - "+propDetails.mondayAMTill);
            } else if(propDetails.mondayAMFrom != undefined) {
                $scope.availableHoursArr.push(propDetails.mondayAMFrom);
            } else if(propDetails.mondayAMTill != undefined) {
                $scope.availableHoursArr.push(propDetails.mondayAMTill);
            }

            if(propDetails.mondayPMFrom != undefined && propDetails.mondayPMTill != undefined) {
                $scope.availableHoursArr.push(propDetails.mondayPMFrom+" - "+propDetails.mondayPMTill);
            } else if(propDetails.mondayPMFrom != undefined) {
                $scope.availableHoursArr.push(propDetails.mondayPMFrom);
            } else if(propDetails.mondayPMTill != undefined) {
                $scope.availableHoursArr.push(propDetails.mondayPMTill);
            }
        } else if(day == 'Tuesday') {
            $scope.availableHoursArr = [];
            if(propDetails.tuesdayAMFrom != undefined && propDetails.tuesdayAMTill != undefined) {
                $scope.availableHoursArr.push(propDetails.tuesdayAMFrom+" - "+propDetails.tuesdayAMTill);
            } else if(propDetails.tuesdayAMFrom != undefined) {
                $scope.availableHoursArr.push(propDetails.tuesdayAMFrom);
            } else if(propDetails.tuesdayAMTill != undefined) {
                $scope.availableHoursArr.push(propDetails.tuesdayAMTill);
            }

            if(propDetails.tuesdayPMFrom != undefined && propDetails.tuesdayPMTill != undefined) {
                $scope.availableHoursArr.push(propDetails.tuesdayPMFrom+" - "+propDetails.tuesdayPMTill);
            } else if(propDetails.tuesdayPMFrom != undefined) {
                $scope.availableHoursArr.push(propDetails.tuesdayPMFrom);
            } else if(propDetails.tuesdayPMTill != undefined) {
                $scope.availableHoursArr.push(propDetails.tuesdayPMTill);
            }
        } else if(day == 'Wednesday') {
            $scope.availableHoursArr = [];
            if(propDetails.wednesdayAMFrom != undefined && propDetails.wednesdayAMTill != undefined) {
                $scope.availableHoursArr.push(propDetails.wednesdayAMFrom+" - "+propDetails.wednesdayAMTill);
            } else if(propDetails.wednesdayAMFrom != undefined) {
                $scope.availableHoursArr.push(propDetails.wednesdayAMFrom);
            } else if(propDetails.wednesdayAMTill != undefined) {
                $scope.availableHoursArr.push(propDetails.wednesdayAMTill);
            }

            if(propDetails.wednesdayPMFrom != undefined && propDetails.wednesdayPMTill != undefined) {
                $scope.availableHoursArr.push(propDetails.wednesdayPMFrom+" - "+propDetails.wednesdayPMTill);
            } else if(propDetails.wednesdayPMFrom != undefined) {
                $scope.availableHoursArr.push(propDetails.wednesdayPMFrom);
            } else if(propDetails.wednesdayPMTill != undefined) {
                $scope.availableHoursArr.push(propDetails.wednesdayPMTill);
            }
        } else if(day == 'Thursday') {
            $scope.availableHoursArr = [];
            if(propDetails.thursdayAMFrom != undefined && propDetails.thursdayAMTill != undefined) {
                $scope.availableHoursArr.push(propDetails.thursdayAMFrom+" - "+propDetails.thursdayAMTill);
            } else if(propDetails.thursdayAMFrom != undefined) {
                $scope.availableHoursArr.push(propDetails.thursdayAMFrom);
            } else if(propDetails.thursdayAMTill != undefined) {
                $scope.availableHoursArr.push(propDetails.thursdayAMTill);
            }

            if(propDetails.thursdayPMFrom != undefined && propDetails.thursdayPMTill != undefined) {
                $scope.availableHoursArr.push(propDetails.thursdayPMFrom+" - "+propDetails.thursdayPMTill);
            } else if(propDetails.thursdayPMFrom != undefined) {
                $scope.availableHoursArr.push(propDetails.thursdayPMFrom);
            } else if(propDetails.thursdayPMTill != undefined) {
                $scope.availableHoursArr.push(propDetails.thursdayPMTill);
            }
        } else if(day == 'Friday') {
            $scope.availableHoursArr = [];
            if(propDetails.fridayAMFrom != undefined && propDetails.fridayAMTill != undefined) {
                $scope.availableHoursArr.push(propDetails.fridayAMFrom+" - "+propDetails.fridayAMTill);
            } else if(propDetails.fridayAMFrom != undefined) {
                $scope.availableHoursArr.push(propDetails.fridayAMFrom);
            } else if(propDetails.fridayAMTill != undefined) {
                $scope.availableHoursArr.push(propDetails.fridayAMTill);
            }

            if(propDetails.fridayPMFrom != undefined && propDetails.fridayPMTill != undefined) {
                $scope.availableHoursArr.push(propDetails.fridayPMFrom+" - "+propDetails.fridayPMTill);
            } else if(propDetails.fridayPMFrom != undefined) {
                $scope.availableHoursArr.push(propDetails.fridayPMFrom);
            } else if(propDetails.fridayPMTill != undefined) {
                $scope.availableHoursArr.push(propDetails.fridayPMTill);
            }
        } else {
            $scope.availableHoursArr = [];
            if(propDetails.saturdayAMFrom != undefined && propDetails.saturdayAMTill != undefined) {
                $scope.availableHoursArr.push(propDetails.saturdayAMFrom+" - "+propDetails.saturdayAMTill);
            } else if(propDetails.saturdayAMFrom != undefined) {
                $scope.availableHoursArr.push(propDetails.saturdayAMFrom);
            } else if(propDetails.saturdayAMTill != undefined) {
                $scope.availableHoursArr.push(propDetails.saturdayAMTill);
            }

            if(propDetails.saturdayPMFrom != undefined && propDetails.saturdayPMTill != undefined) {
                $scope.availableHoursArr.push(propDetails.saturdayPMFrom+" - "+propDetails.saturdayPMTill);
            } else if(propDetails.saturdayPMFrom != undefined) {
                $scope.availableHoursArr.push(propDetails.saturdayPMFrom);
            } else if(propDetails.saturdayPMTill != undefined) {
                $scope.availableHoursArr.push(propDetails.saturdayPMTill);
            }
        }
        $scope.options = ['var1', 'var2', 'var3'];
        if($scope.availableHoursArr.length > 0) {
            // console.log($scope.availableHoursArr);
            var timeSelectContent = '<select ng-model="appointmentTime"><option value="">Select Time</option>';
            for(var i = 0; i < $scope.availableHoursArr.length; i++) {
                timeSelectContent += '<option value="'+$scope.availableHoursArr[i]+'">'+$scope.availableHoursArr[i]+'</option>';
            }
            timeSelectContent += '</select>';
            // $scope.showAvailableHours = true;
            var $el = $(timeSelectContent);
            $compile($el)($scope);              
            $("#timeSelectBox").html($el);
            $("#timeSelectBox").show();
        }
    });

    $scope.updateAppointment = function() {
        var dateFormat = $("#contactDate").val().split("-").reverse().join("-");
        var postData = '{"appointmentID":"'+$scope.propAppointments.appointmentID+'","appointmentOn":"'+dateFormat+'","appointmentTime":"'+$scope.appointmentTime+'","appointmentFrom":"'+$scope.userID+'","appointmentTo":"'+$scope.propDetails.userID+'","propetyID":"'+$scope.propDetails.propertyID+'","appointmentStatus":"Reschedule","appointmentNotes":"","isConvertToLead":""}';
            
        console.log(postData);
        var req = {
            method: 'POST',
            url: domainAddress+'UpdateAppointment',
            data: postData
        }
        $http(req).then(function(resp) {
            var response = resp.data;
            if(response.status === "success"){
                $scope.propAppointments.appointmentStatus="";
                $scope.propAppointments.appointmentDate = dateFormat;
                $scope.propAppointments.appointmentTime = $scope.appointmentTime;
                $("#rescheduleModal").modal("toggle");
                $scope.showContactBtn = false;
                swal("Success!", "Appointment Updated!", "success")
            } else {
                console.log("Error in updating appointment");
            }
        });
    }

    $scope.updateVisited = function(appointment) {
        appointment.appointmentStatus = 'Rated';
        $scope.appointment = appointment;
        var req = {
            method: 'POST',
            url: domainAddress+'UpdateRated/'+appointment.appointmentID,
        }
        $http(req).then(function(resp) {
            var response = resp.data;
            if(response.status === "success") {
                $("#modal-appointments").modal("toggle");
                swal("Success!", "Appointment Updated!", "success")
            } else {
                console.log("Error in updating appointment");
            }
        });
    }

    if( $('.ui-slider').length > 0 ){
        $('.ui-slider').each(function() {
            if( $("body").hasClass("rtl") ) var rtl = "rtl";
            else rtl = "ltr";

            var step;
            if( $(this).attr('data-step') ) {
                step = parseInt( $(this).attr('data-step') );
            }
            else {
                step = 1000;
            }
            var sliderElement = $(this).attr('id');
            var element = $( '#' + sliderElement);
            var valueMin = parseInt( $(this).attr('data-value-min') );
            var valueMax = parseInt( $(this).attr('data-value-max') );
            $(this).noUiSlider({
                start: [ valueMin, valueMax ],
                connect: true,
                direction: rtl,
                range: {
                    'min': valueMin,
                    'max': valueMax
                },
                step: step
            });
            if( $(this).attr('data-value-type') == 'price' ) {
                if( $(this).attr('data-currency-placement') == 'before' ) {
                    $(this).Link('lower').to( $(this).children('.values').children('.value-min'), null, wNumb({ prefix: $(this).attr('data-currency'), decimals: 0, thousand: '.' }));
                    $(this).Link('upper').to( $(this).children('.values').children('.value-max'), null, wNumb({ prefix: $(this).attr('data-currency'), decimals: 0, thousand: '.' }));
                }
                else if( $(this).attr('data-currency-placement') == 'after' ){
                    $(this).Link('lower').to( $(this).children('.values').children('.value-min'), null, wNumb({ postfix: $(this).attr('data-currency'), decimals: 0, thousand: ' ' }));
                    $(this).Link('upper').to( $(this).children('.values').children('.value-max'), null, wNumb({ postfix: $(this).attr('data-currency'), decimals: 0, thousand: ' ' }));
                }
            }
            else {
                $(this).Link('lower').to( $(this).children('.values').children('.value-min'), null, wNumb({ decimals: 0 }));
                $(this).Link('upper').to( $(this).children('.values').children('.value-max'), null, wNumb({ decimals: 0 }));
            }
        });
    }

    $scope.textSearchFunction = function(searchText) {
        if(searchText != undefined && searchText != '') {
            var req = {
                method: 'POST',
                url: domainAddress+'GlobalSearchProperty/'+searchText,
            }
            $http(req).then(function(resp) {
                var response = resp.data;
                if(response.status === "success"){
                    var records = response.records;
                    for(var i = 0; i < records.length; i++) {
                        if(records[i].propertyImages != null && records[i].propertyImages != undefined) {
                            for(var j = 0; j < records[i].propertyImages.length; j++) {
                                records[i].propertyImages[j] = domainAddress+records[i].propertyImages[j];
                            }
                        }
                    }
                    $scope.resultAllProperties = records;
                    var x = 0;
                    for(var i = 0; i < records.length; i++) {
                        if(records[i].appointments != undefined) {
                            var appointments = records[i].appointments;
                            for(var j = 0; j < appointments.length; j++) {
                                if($scope.userID == appointments[j].appointmentFrom) {
                                    $scope.appointment = appointments[j];
                                    $scope.resultAllProperties[i].appointmentUser = appointments[j];
                                }
                            }
                        }
                    }
                    $scope.noProperty = false;
                } else {
                    console.log("Failed");
                    $scope.noProperty = true;
                }
            });
        } else {
            var req = {
                method: 'GET',
                url: domainAddress+'GetAllPropertyList',
            }
            $http(req).then(function(resp) {
                var response = resp.data;
                if(response.status === "success"){
                    var records = response.records;
                    for(var i = 0; i < records.length; i++) {
                        if(records[i].propertyImages != null && records[i].propertyImages != undefined) {
                            for(var j = 0; j < records[i].propertyImages.length; j++) {
                                records[i].propertyImages[j] = domainAddress+records[i].propertyImages[j];
                            }
                        }
                    }
                    $scope.resultAllProperties = records;
                    var x = 0;
                    for(var i = 0; i < records.length; i++) {
                        if(records[i].appointments != undefined) {
                            var appointments = records[i].appointments;
                            for(var j = 0; j < appointments.length; j++) {
                                if($scope.userID == appointments[j].appointmentFrom) {
                                    $scope.appointment = appointments[j];
                                        $scope.resultAllProperties[i].appointmentUser = appointments[j];
                                }
                            }
                        }
                    }
                    $scope.noProperty = false;
                } else {
                    console.log("Failed");
                    $scope.noProperty = true;
                }
            });
        }
    }
    $scope.saveProperty = function(propID) {
        var postData = '{"propertyID":"'+propID+'","userID":"'+$scope.userID+'"}';
        var req = {
            method: 'POST',
            url: domainAddress+'SaveProperty',
            data: postData
        }
        $http(req).then(function(resp) {
            var response = resp.data;
            if(response.status === "Success"){
               setTimeout(() => {
                    swal("Success!", "Property Saved", "success");
                },300);
            } else {
                console.log("Error in creating appointment");
            }
        });
    }
    $scope.onFavClick = function() {
        $("#BrowseProp").removeAttr("class");
        $("#FavProps").addClass("active");
        $scope.showFavourites = true;

        var req = {
            method: 'POST',
            url: domainAddress+'GetFavouriteProperties/'+$scope.userID,
        }
        $http(req).then(function(resp) {
            var response = resp.data;
            if(response.status === "success"){
                var records = response.records;
                for(var i = 0; i < records.length; i++) {
                    if(records[i].propertyImages != null && records[i].propertyImages != undefined) {
                        for(var j = 0; j < records[i].propertyImages.length; j++) {
                            records[i].propertyImages[j] = domainAddress+records[i].propertyImages[j];
                        }
                    }
                }
                $scope.favouriteProps = records;
                var x = 0;
                for(var i = 0; i < records.length; i++) {
                    if(records[i].appointments != undefined) {
                        var appointments = records[i].appointments;
                        for(var j = 0; j < appointments.length; j++) {
                            if($scope.userID == appointments[j].appointmentFrom) {
                                $scope.appointment = appointments[j];
                                    $scope.resultAllProperties[i].appointmentUser = appointments[j];
                            }
                        }
                    }
                }
                $scope.noProperty = false;
            } else {
                console.log("Failed");
                $scope.noProperty = true;
            }
        });
    }
    $scope.onBrowseClick = function() {
        console.log($scope.userID);
        $scope.showFavourites = false;

        var req = {
            method: 'GET',
            url: domainAddress+'GetAllPropertyList',
        }
        $http(req).then(function(resp) {
            var response = resp.data;
            if(response.status === "success"){
                var records = response.records;
                for(var i = 0; i < records.length; i++) {
                    if(records[i].propertyImages != null && records[i].propertyImages != undefined) {
                        for(var j = 0; j < records[i].propertyImages.length; j++) {
                            records[i].propertyImages[j] = domainAddress+records[i].propertyImages[j];
                        }
                    }
                }
                $scope.resultAllProperties = records;
                var x = 0;
                for(var i = 0; i < records.length; i++) {
                    if(records[i].appointments != undefined) {
                        var appointments = records[i].appointments;
                        for(var j = 0; j < appointments.length; j++) {
                            if($scope.userID == appointments[j].appointmentFrom) {
                                $scope.appointment = appointments[j];
                                    $scope.resultAllProperties[i].appointmentUser = appointments[j];
                            }
                        }
                    }
                }
                $scope.noProperty = false;
            } else {
                console.log("Failed");
                $scope.noProperty = true;
            }
        });
    }
});