app.controller('addPropertyController', function($timeout, $http, $rootScope, $window, $scope, $sce) {

    $(".select2").select2();
    $(".select2").attr("style","width: 100%");

    $scope.userID = '';
    $scope.listStates = [];
    $scope.propertySuccess = false;
    var getZingyMoveUserData = JSON.parse(localStorage.getItem("zingyMoveUserInfo"));
    if(getZingyMoveUserData!=null){
        $scope.userID = getZingyMoveUserData[0].userID;
        $scope.userType = getZingyMoveUserData[0].userType;
    }
    $scope.descModalClose = false;
    var _latitude = 51.507351;
    var _longitude = -0.127758;
    var element = "map-submit";
    
    if($scope.userType == 'Admin') {
        var req = {
            method: 'POST',
            url: domainAddress+'GetAdminBasedUsers/'+$scope.userID
        }
        $http(req).then(function(resp) {
            var response = resp.data;
            if(response.status === "success"){
                if(resp.data.record_count > 0) {
                    $scope.AllUsers = resp.data.records;
                } else {
                    $scope.noUsers = true;
                }
            } else {
                swal("Oops!", "No Owners Assigned!", "info");
            }
        });
    }
    loadStates('+44');
    simpleMap(_latitude,_longitude, element, true);

    alldaysInitialCheck();

    $scope.errorOwnerSelect = false;
    $scope.errorPropertyTitle = false;
    $scope.errorPropertyType = false;
    $scope.errorPropertyDescription = false;
    $scope.errorPropertyAddress = false;
    $scope.errorCity = false;
    $scope.errorPropPincode = false;
    $scope.errorCountry = false;
    $scope.errorState = false;
    $scope.errorPropertyPrice = false;
    $scope.errorPropertyFacing = false;
    $scope.errorPropertyLatLong = false;
    $scope.errorPropImages = false;
    $scope.errorPropertyFilter = false;

    $scope.errorFloorCovering = false;
    $scope.errorInternalCondition = false;
    $scope.errorExternalCondition = false;

    $scope.errorPropertyAge = false;
    $scope.errorNoOfParking = false;
    $scope.errorNoOfBedRooms = false;

    $scope.errorNoOfBathRooms = false;
    $scope.errorNoOfStudyRooms = false;
    $scope.errorNoOfReceptionRooms = false;

    $scope.errorCellarBasement = false;
    $scope.errorCableSatellite = false;
    $scope.errorBlindsCurtains = false;

    $('#modal-description').on('hidden.bs.modal', function () {
        var desc = $("#propertyDescription").val();
        if(desc == '') {
            $("#propertyDescription").focus();
            $scope.descModalClose = true;
        }
    });

    $scope.onTextDescFocus = function() {
        var desc = $("#propertyDescription").val();
        if(desc == '') {
            setTimeout(() => {
                if($scope.descModalClose == false) {
                    $("#modal-description").modal("toggle");
                }
            },500);
        }
    }

    $scope.selectDesc = function(block) {
        if(block == 1) {
            $scope.propertyDescription = $("#desc1").html().replace(/\s\s+/g, ' ');
            $("#modal-description").modal("toggle");
        } else if(block == 2) {
            $scope.propertyDescription = $("#desc2").html().replace(/\s\s+/g, ' ');
            $("#modal-description").modal("toggle");
        } else {
            $scope.propertyDescription = $("#desc3").html().replace(/\s\s+/g, ' ');
            $("#modal-description").modal("toggle");
        }
    }
    $("#descriptionTrigger").on("click", function() {
        $("#modal-description").modal("toggle");
    });

    // $scope.onOwnerSelectChange = function(value) {
    //     if(value == '' || value == undefined) {
    //         $scope.errorOwnerSelect = true;
    //         $scope.ownerSelect = '';
    //     } else if(value=="addOwner") {
    //         $scope.errorOwnerSelect = false;
    //         $("#modal-add-owners").modal("toggle");
    //         $scope.ownerSelect = '';
    //     } else {
    //         $scope.errorOwnerSelect = false;
    //         $scope.ownerSelect = value;
    //     }
    // }

    $scope.onOwnerSelectChange = function(value) {
        if(value == '' || value == undefined) {
            $scope.errorOwnerSelect = true;
            $scope.ownerSelect = '';
            $("#errorOwnerSelect").html("*Select Owner");
        } else if(value=="addOwner") {
            $scope.errorOwnerSelect = false;
            $("#modal-add-owners").modal("toggle");
            $scope.ownerSelect = '';
        } else {
            $scope.errorOwnerSelect = false;
            $("#errorOwnerSelect").html("");
            $scope.ownerSelect = value;
        }
    }

    $scope.onPropertyTitleChange = function(value) {
        if(value == '' || value == undefined) {
            $scope.errorPropertyTitle = true;
        } else {
            $scope.errorPropertyTitle = false;
        }
    }

    $scope.onPropertyTypeChange = function(value) {
        if(value == '' || value == undefined) {
            $scope.errorPropertyType = true;
        } else {
            $scope.errorPropertyType = false;
        }

        var desc = $("#propertyDescription").val();
        if(desc == '') {
            setTimeout(() => {
                if($scope.descModalClose == false) {
                    $("#modal-description").modal("toggle");
                }
            },500);
        }
    }

    $scope.onPropertyDescriptionChange = function(value) {
        if(value == '' || value == undefined) {
            $scope.errorPropertyDescription = true;
        } else {
            $scope.errorPropertyDescription = false;
        }
    }

    $scope.onPropertyAddressChange = function(value) {
        if(value == '' || value == undefined) {
            $scope.errorPropertyAddress = true;
        } else {
            $scope.errorPropertyAddress = false;
        }
    }

    // $scope.onPropLatLongChange = function() {
    //     // var propLat = $("#latitude").val();
    //     // var propLong = $("#longitude").val();
    //     // if(propLat == '' || propLong == '') {
    //     //     $scope.errorPropertyLatLong = true;
    //     // } else {
    //     //     $scope.errorPropertyLatLong = false;            
    //     // }
    // }

    $scope.onCityChange = function(value) {
        if(value == '' || value == undefined) {
            $scope.errorCity = true;
            $("#errorCity").html("*Select Property City");
        } else {
            $scope.errorCity = false;
            $("#errorCity").html("");
        }
    }

    $scope.onPinCodeChange = function(value) {
        if(value == '' || value == undefined) {
            $scope.errorPropPincode = true;
        } else {
            $scope.errorPropPincode = false;
        }
    }

    // $scope.onCountryChange = function(value) {
    //     if(value == '' || value == undefined) {
    //         $scope.errorCountry = true;
    //     } else {
    //         $scope.errorCountry = false;
    //     }
        
    //     if(value == 'India') {
    //         loadStates('+91');
    //     } else {
    //         loadStates('+44');
    //     }
    // }

    $scope.onStateChange = function(value) {
        if(value == '' || value == undefined) {
            $scope.errorState = true;
            $("#errorState").html("*Select County");
        } else {
            $scope.errorState = false;
            $("#errorState").html("");
            
        }

        // loadCities(JSON.parse(value).StateName);
        loadCities(value);
    }

    $scope.onPriceChange = function(value) {
        if(value == '' || value == undefined) {
            $scope.errorPropertyPrice = true;
        } else {
            $scope.errorPropertyPrice = false;
        }
    }

    $scope.onPropertyFacingChange = function(value) {
        if(value == '' || value == undefined) {
            $scope.errorPropertyFacing = true;
        } else {
            $scope.errorPropertyFacing = false;
        }
    }

    // $scope.onPropImageChange = function(value) {
    //     if(value == '' || value == undefined) {
    //         $scope.errorPropImages = true;
    //     } else {
    //         $scope.errorPropImages = false;
    //     }
    // }

    $scope.onPropFilterChange = function(value) {
        if(value == '' || value == undefined) {
            $scope.errorPropertyFilter = true;
        } else {
            $scope.errorPropertyFilter = false;
        }
    }

    $scope.onFloorCoveringChange = function(value) {
        if(value == '' || value == undefined) {
            $scope.errorFloorCovering = true;
        } else {
            $scope.errorFloorCovering = false;
        }
    }

    $scope.onInternalConditionChange = function(value) {
        if(value == '' || value == undefined) {
            $scope.errorInternalCondition = true;
        } else {
            $scope.errorInternalCondition = false;
        }
    }

    $scope.onExternalConditionChange = function(value) {
        if(value == '' || value == undefined) {
            $scope.errorExternalCondition = true;
        } else {
            $scope.errorExternalCondition = false;
        }
    }
    
    $scope.onPropertyAgeChange = function(value) {
        if(value == '' || value == undefined) {
            $scope.errorPropertyAge = true;
        } else {
            $scope.errorPropertyAge = false;
        }
    }

    $scope.onNoOfParkingChange = function(value) {
        if(value == '' || value == undefined) {
            $scope.errorNoOfParking = true;
        } else {
            $scope.errorNoOfParking = false;
        }
    }

    $scope.onNoOfBedRoomsChange = function(value) {
        if(value == '' || value == undefined) {
            $scope.errorNoOfBedRooms = true;
        } else {
            $scope.errorNoOfBedRooms = false;
        }
    }

    $scope.onNoOfBathRoomsChange = function(value) {
        if(value == '' || value == undefined) {
            $scope.errorNoOfBathRooms = true;
        } else {
            $scope.errorNoOfBathRooms = false;
        }
    }

    $scope.onNoOfStudyRoomsChange = function(value) {
        if(value == '' || value == undefined) {
            $scope.errorNoOfStudyRooms = true;
        } else {
            $scope.errorNoOfStudyRooms = false;
        }
    }

    $scope.onNoOfReceptionRoomsChange = function(value) {
        if(value == '' || value == undefined) {
            $scope.errorNoOfReceptionRooms = true;
        } else {
            $scope.errorNoOfReceptionRooms = false;
        }
    }

    $scope.onCellarBasementChange = function(value) {
        if(value == '' || value == undefined) {
            $scope.errorCellarBasement = true;
        } else {
            $scope.errorCellarBasement = false;
        }
    }

    $scope.onCableSatelliteChange = function(value) {
        if(value == '' || value == undefined) {
            $scope.errorCableSatellite = true;
        } else {
            $scope.errorCableSatellite = false;
        }
    }

    $scope.onBlindsCurtainsChange = function(value) {
        if(value == '' || value == undefined) {
            $scope.errorBlindsCurtains = true;
        } else {
            $scope.errorBlindsCurtains = false;
        }
    }

    $scope.AddUser = function() {
        if($scope.addusertitle == '' || $scope.addusertitle == undefined) {
            $scope.errorUsertitle = true;
        } else if($scope.addusername == '' || $scope.addusername == undefined) {
            $scope.errorUserName = true;
        } else if($scope.adduserMail == '' || $scope.adduserMail == undefined) {
            $scope.errorUserMail = true;
        } else if($scope.adduserPhone == '' || $scope.adduserPhone == undefined) {
            $scope.errorUserPhone = true;
        } else if(isNaN($scope.adduserPhone)) {
            $scope.errorUserPhone = true;
        } else { 
            var postdata = '{"userTitle":"'+$scope.addusertitle+'","userName":"'+$scope.addusername+'","userEmail": "'+$scope.adduserMail+'","userPhone": "'+$scope.adduserPhone+'","userPassword": "Password#1","userType": "Owner","adminID":"'+$scope.userID+'"}';
            var req = {
                method: 'POST',
                url: domainAddress+'CreateUser',
                data: postdata
            }
            $http(req).then(function(resp) {
                var response = resp.data;
                if(response.status === "Success"){
                    swal("Success!", "User Added Successfully!", "success");
                    $("#modal-add-owners").modal("toggle");
                    var req = {
                        method: 'POST',
                        url: domainAddress+'GetAdminBasedUsers/'+$scope.userID
                    }
                    $http(req).then(function(resp) {
                        var response = resp.data;
                        if(response.status === "success"){
                            if(resp.data.record_count > 0) {
                                $scope.AllUsers = resp.data.records;
                            } else {
                                $scope.noUsers = true;
                            }
                        } else {
                            swal("Oops!", "No Owners Assigned!", "info");
                        }
                    });
                    $('#userForm')[0].reset();
                    $("html, body").animate({ scrollTop: 0 }, "slow");
                    $scope.addusertitle = '';
                    $scope.addusername = '';
                    $scope.adduserMail = '';
                    $scope.adduserPhone = '';
                    $scope.addpassword = '';
                    $scope.adduserType = '';                    
                } else if(response.status === 'Exists') {
                    swal("Oops!", "User Already Exists!", "warning");
                } else {
                    swal("Oops!", "Something went wrong!", "error");
                }
            });
        }
    }

    $scope.addProperty = function () {
        // $('#submitAddPropBtn').prop('disabled', true);

        var propertyLat = $("#latitude").val();
        var propertyLong = $("#longitude").val();
        var propImageIds = $("#propImageIds").val();
        var pincodeVal = $("#pincode").val();


        if($scope.propertyTitle == '' || $scope.propertyTitle == undefined) {
            $scope.errorPropertyTitle = true;
            $("#propertyTitle").focus();
            return false;
        } else if($scope.propertyType == '' || $scope.propertyType == undefined) {
            $scope.errorPropertyType = true;
            $("#propertyType").focus();            
            return false;
        } else if($scope.propertyDescription == '' || $scope.propertyDescription == undefined) {
            $scope.errorPropertyDescription = true;
            $("#propertyDescription").focus();
            return false;
            
        } else if($scope.propertyAddress == '' || $scope.propertyAddress == undefined) {
            $scope.errorPropertyAddress = true;
            $("#address-autocomplete").focus();
            return false;
            
        } else if($scope.state == '' || $scope.state == undefined) {
            $scope.errorState = true;
            $("#errorState").html("*Select County");
            $("#state").focus();
            return false;
            
        } else if($scope.City == '' || $scope.City == undefined) {
            $scope.errorCity = true;
            $("#errorCity").html("*Select Property City");
            $("#City").focus();
            return false;
            
        } else if($("#pincode").val() == '' || $("#pincode").val() == undefined) {
            $scope.errorPropPincode = true;
            $("#pincode").focus();
            return false;
            
        } else if($scope.propertyPrice == '' || $scope.propertyPrice == undefined) {
            $scope.errorPropertyPrice = true;
            $("#propertyPrice").focus();
            return false;
            
        } else if($scope.propertyFacing == '' || $scope.propertyFacing == undefined) {
            $scope.errorPropertyFacing = true;
            $("#propertyFacing").focus();
            return false;
            
        // } else if($scope.propertyLat == '' || propertyLat == undefined || propertyLong == '' || propertyLong == undefined) {
        //     $scope.errorPropertyLatLong = true;
        //     $("#map-submit").focus();
        //     return false;
            
        // } else if(propImageIds == '' || propImageIds == undefined) {
        //     $scope.errorPropImages = true;
        //     $("#dropbox").focus();
        //     return false;
            
        } else if($scope.propertyFilter == '' || $scope.propertyFilter == undefined) {
            $scope.errorPropertyFilter = true;
            $("#propertyFilter").focus();
            return false;
            
        } else if($scope.floorCovering == '' || $scope.floorCovering == undefined) {
            $scope.errorFloorCovering = true;
            $("#floorCovering").focus();
            return false;
            
        } else if($scope.internalCondition == '' || $scope.internalCondition == undefined) {
            $scope.errorInternalCondition = true;
            $("#internalCondition").focus();
            return false;
            
        } else if($scope.externalCondition == '' || $scope.externalCondition == undefined) {
            $scope.errorExternalCondition = true;
            $("#externalCondition").focus();
            return false;
            
        } else if($scope.propertyAge == '' || $scope.propertyAge == undefined) {
            $scope.errorPropertyAge = true;
            $("#propertyAge").focus();
            return false;
            
        } else if($scope.noOfParking == '' || $scope.noOfParking == undefined) {
            $scope.errorNoOfParking = true;
            $("#noOfParking").focus();
            return false;
            
        } else if($scope.noOfBedRooms == '' || $scope.noOfBedRooms == undefined) {
            $scope.errorNoOfBedRooms = true;
            $("#noOfBedRooms").focus();
            return false;
            
        } else if($scope.noOfBathRooms == '' || $scope.noOfBathRooms == undefined) {
            $scope.errorNoOfBathRooms = true;
            $("#noOfBathRooms").focus();
            return false;
            
        } else if($scope.noOfStudyRooms == '' || $scope.noOfStudyRooms == undefined) {
            $scope.errorNoOfStudyRooms = true;
            $("#noOfStudyRooms").focus();
            return false;
            
        } else if($scope.noOfReceptionRooms == '' || $scope.noOfReceptionRooms == undefined) {
            $scope.errorNoOfReceptionRooms = true;
            $("#noOfReceptionRooms").focus();
            return false;
            
        } else if($scope.cellarBasement == '' || $scope.cellarBasement == undefined) {
            $scope.errorCellarBasement = true;
            $("#cellarBasement").focus();
            return false;
            
        } else if($scope.cableSatellite == '' || $scope.cableSatellite == undefined) {
            $scope.errorCableSatellite = true;
            $("#cableSatelite").focus();
            return false;
            
        } else if($scope.blindsCurtains == '' || $scope.blindsCurtains == undefined) {
            $scope.errorBlindsCurtains = true;
            $("#blindsCurtains").focus();
            return false;
            
        } else if($scope.mondayCheckedFalse && $scope.tuesdayCheckedFalse && $scope.wednesdayCheckedFalse && $scope.thursdayCheckedFalse && $scope.fridayCheckedFalse && $scope.saturdayCheckedFalse && $scope.sundayCheckedFalse) {
            $scope.errorOpeningHours = true;
            $('#accordion').focus();
        } else {

            if($scope.isFrontGarden == undefined) {
                $scope.isFrontGarden = 0;
            }
            if($scope.isRearGarden == undefined) {
                $scope.isRearGarden = 0;
            }
            if($scope.isExtractorFanBathroom == undefined) {
                $scope.isExtractorFanBathroom = 0;
            }
            if($scope.isSeperateWC == undefined) {
                $scope.isSeperateWC = 0;
            }
            if($scope.isEnsuite == undefined) {
                $scope.isEnsuite = 0;
            }
            if($scope.utilityRoom == undefined) {
                $scope.utilityRoom = 0;
            }
            if($scope.heatingGas == undefined) {
                $scope.heatingGas = 0;
            }
            if($scope.heatingElectricty == undefined) {
                $scope.heatingElectricty = 0;
            }
            if($scope.smokeAlarm == undefined) {
                $scope.smokeAlarm = 0;
            }
            if($scope.monoxideAlarm == undefined) {
                $scope.monoxideAlarm = 0;
            }
            if($scope.chimney == undefined) {
                $scope.chimney = 0;
            }
            if($scope.saniflo == undefined) {
                $scope.saniflo = 0;
            }
            if($scope.doubleGlazing == undefined) {
                $scope.doubleGlazing = 0;
            }
            if($scope.isKitchenWashingMachin == undefined) {
                $scope.isKitchenWashingMachin = 0;
            }
            if($scope.isKitchenTumbleDryer == undefined) {
                $scope.isKitchenTumbleDryer = 0;
            }
            if($scope.isKitchenOven == undefined) {
                $scope.isKitchenOven = 0;
            }
            if($scope.isKitchenHob == undefined) {
                $scope.isKitchenHob = 0;
            }
            if($scope.isExtractorFan == undefined) {
                $scope.isExtractorFan = 0;
            }

            var propertyAddress = $("#address-autocomplete").val();

            var openingHours = '"isMonday":'+$scope.isMonday+',"isTuesday":'+$scope.isTuesday+',"isWednesday":'+$scope.isWednesday+',"isThursday":'+$scope.isThursday+',"isFriday":'+$scope.isFriday+',"isSaturday":'+$scope.isSaturday+',"isSunday":'+$scope.isSunday+',"mondayAMFrom":"'+$("#mondayAMFrom").val()+'","mondayAMTill":"'+$("#mondayAMTill").val()+'","mondayPMFrom":"'+$("#mondayPMFrom").val()+'","mondayPMTill":"'+$("#mondayPMTill").val()+'","tuesdayAMFrom":"'+$("#tuesdayAMFrom").val()+'","tuesdayAMTill":"'+$("#tuesdayAMTill").val()+'","tuesdayPMFrom":"'+$("#tuesdayPMFrom").val()+'","tuesdayPMTill":"'+$("#tuesdayPMTill").val()+'","wednesdayAMFrom":"'+$("#wednesdayAMFrom").val()+'","wednesdayAMTill":"'+$("#wednesdayAMTill").val()+'","wednesdayPMFrom":"'+$("#wednesdayPMFrom").val()+'","wednesdayPMTill":"'+$("#wednesdayPMTill").val()+'","thursdayAMFrom":"'+$("#thursdayAMFrom").val()+'","thursdayAMTill":"'+$("#thursdayAMTill").val()+'","thursdayPMFrom":"'+$("#thursdayPMFrom").val()+'","thursdayPMTill":"'+$("#thursdayPMTill").val()+'","fridayAMFrom":"'+$("#fridayAMFrom").val()+'","fridayAMTill":"'+$("#fridayAMTill").val()+'","fridayPMFrom":"'+$("#fridayPMFrom").val()+'","fridayPMTill":"'+$("#fridayPMTill").val()+'","saturdayAMFrom":"'+$("#saturdayAMFrom").val()+'","saturdayAMTill":"'+$("#saturdayAMTill").val()+'","saturdayPMFrom":"'+$("#saturdayPMFrom").val()+'","saturdayPMTill":"'+$("#saturdayPMTill").val()+'","sundayAMFrom":"'+$("#sundayAMFrom").val()+'","sundayAMTill":"'+$("#sundayAMTill").val()+'","sundayPMFrom":"'+$("#sundayPMFrom").val()+'","sundayPMTill":"'+$("#sundayPMTill").val()+'"';

            postData = '';

            if($scope.userType == 'Admin') {
                if($scope.ownerSelect == '' || $scope.ownerSelect == undefined) {
                    $scope.errorOwnerSelect = true;
                    $("#ownerSelect").focus();
                    return false;
                } else {
                    postData = '{"propertyTitle":"'+$scope.propertyTitle+'","propertyDesc":"'+$scope.propertyDescription+'","propertyAddress":"'+propertyAddress+'","propertyLat":"'+propertyLat+'","propertyLong":"'+propertyLong+'","propertyType":"'+$scope.propertyType+'","propertyFilter":"'+$scope.propertyFilter+'","propertyPrice":"'+$scope.propertyPrice+'","propertyCounty":"'+$scope.state+'","propertyCity":"'+$scope.City+'","propertyPostCode":"'+pincodeVal+'","bedroom":"'+$scope.noOfBedRooms+'","bathroom":"'+$scope.noOfBathRooms+'","studyRoom":"'+$scope.noOfStudyRooms+'","floorCovering":"'+$scope.floorCovering+'","flooringCoveringother":"","userID":"'+$scope.ownerSelect+'","isAvailable":"1","propertAge":"'+$scope.propertyAge+'","parkingNumber":"'+$scope.noOfParking+'","isFrontGarden":'+$scope.isFrontGarden+',"isRearGarden":'+$scope.isRearGarden+',"isExtractorFanBathroom":'+$scope.isExtractorFanBathroom+',"isSeperateWC":'+$scope.isSeperateWC+',"isEnsuite":'+$scope.isEnsuite+',"receptionRooms":"'+$scope.noOfReceptionRooms+'","studyRooms":"'+$scope.noOfStudyRooms+'","utilityRoom":'+$scope.utilityRoom+',"cellarBasement":"'+$scope.cellarBasement+'","heatingGas":'+$scope.heatingGas+',"heatingElectricty":'+$scope.heatingElectricty+',"heatingOther":"0","smokeAlarm":'+$scope.smokeAlarm+',"monoxideAlarm":'+$scope.monoxideAlarm+',"chimney":'+$scope.chimney+',"saniflo":'+$scope.saniflo+',"doubleGlazing":'+$scope.doubleGlazing+',"cableSatellite":"'+$scope.cableSatellite+'","internalCondition":"'+$scope.internalCondition+'","externalCondition":"'+$scope.externalCondition+'","blindsCurtains":"'+$scope.blindsCurtains+'","isKitchenWashingMachin":'+$scope.isKitchenWashingMachin+',"isKitchenTumbleDryer":'+$scope.isKitchenTumbleDryer+',"isKitchenOven":'+$scope.isKitchenOven+',"isKitchenHob":'+$scope.isKitchenHob+',"isExtractorFan":'+$scope.isExtractorFan+',"propertyFacing":"'+$scope.propertyFacing+'","propertyImageIds":"'+propImageIds+'",'+openingHours+'}';
                }
            } else {
                postData = '{"propertyTitle":"'+$scope.propertyTitle+'","propertyDesc":"'+$scope.propertyDescription+'","propertyAddress":"'+propertyAddress+'","propertyLat":"'+propertyLat+'","propertyLong":"'+propertyLong+'","propertyType":"'+$scope.propertyType+'","propertyFilter":"'+$scope.propertyFilter+'","propertyPrice":"'+$scope.propertyPrice+'","propertyCounty":"'+$scope.state+'","propertyCity":"'+$scope.City+'","propertyPostCode":"'+pincodeVal+'","bedroom":"'+$scope.noOfBedRooms+'","bathroom":"'+$scope.noOfBathRooms+'","studyRoom":"'+$scope.noOfStudyRooms+'","floorCovering":"'+$scope.floorCovering+'","flooringCoveringother":"","userID":"'+$scope.userID+'","isAvailable":"1","propertAge":"'+$scope.propertyAge+'","parkingNumber":"'+$scope.noOfParking+'","isFrontGarden":'+$scope.isFrontGarden+',"isRearGarden":'+$scope.isRearGarden+',"isExtractorFanBathroom":'+$scope.isExtractorFanBathroom+',"isSeperateWC":'+$scope.isSeperateWC+',"isEnsuite":'+$scope.isEnsuite+',"receptionRooms":"'+$scope.noOfReceptionRooms+'","studyRooms":"'+$scope.noOfStudyRooms+'","utilityRoom":'+$scope.utilityRoom+',"cellarBasement":"'+$scope.cellarBasement+'","heatingGas":'+$scope.heatingGas+',"heatingElectricty":'+$scope.heatingElectricty+',"heatingOther":"0","smokeAlarm":'+$scope.smokeAlarm+',"monoxideAlarm":'+$scope.monoxideAlarm+',"chimney":'+$scope.chimney+',"saniflo":'+$scope.saniflo+',"doubleGlazing":'+$scope.doubleGlazing+',"cableSatellite":"'+$scope.cableSatellite+'","internalCondition":"'+$scope.internalCondition+'","externalCondition":"'+$scope.externalCondition+'","blindsCurtains":"'+$scope.blindsCurtains+'","isKitchenWashingMachin":'+$scope.isKitchenWashingMachin+',"isKitchenTumbleDryer":'+$scope.isKitchenTumbleDryer+',"isKitchenOven":'+$scope.isKitchenOven+',"isKitchenHob":'+$scope.isKitchenHob+',"isExtractorFan":'+$scope.isExtractorFan+',"propertyFacing":"'+$scope.propertyFacing+'","propertyImageIds":"'+propImageIds+'",'+openingHours+'}';
            }
            if(postData != '') {
                var req = {
                    method: 'POST',
                    url: domainAddress+'CreateProperty',
                    data: postData
                }
                $http(req).then(function(resp) {
                    $scope.spinnerload='';
                    var response = resp.data;
                    if(response.status === "Success"){
                        $('#addPropertyForm')[0].reset();
                        $scope.progressVisible = false;
                        $("#uploadProgress").hide();
                        $("#upload_Preview").html('');
                        $("html, body").animate({ scrollTop: 0 }, "slow");
                        // $scope.propertySuccess = true;
    
                        $scope.propertyTitle = '';
                        $scope.propertyType = '';
                        $scope.propertyDescription = '';
                        $scope.propertyAddress = '';
                        $scope.state = '';
                        $scope.City = '';
                        $scope.propPincode = '';
                        $scope.propertyPrice = ''
                        $scope.propertyFacing = ''
                        $scope.propertyFilter = '';
                        $scope.floorCovering = '';
                        $scope.internalCondition = '';
                        $scope.externalCondition = '';
                        $scope.propertyAge = '';
                        $scope.noOfParking = '';
                        $scope.noOfBedRooms = '';
                        $scope.noOfBathRooms = '';
                        $scope.noOfStudyRooms = '';
                        $scope.noOfReceptionRooms = '';
                        $scope.cellarBasement = '';
                        $scope.cableSatellite = '';
                        $scope.blindsCurtains = '';
    
                        $scope.errorPropertyTitle = false;
                        $scope.errorPropertyType = false;
                        $scope.errorPropertyDescription = false;
                        $scope.errorPropertyAddress = false;
                        $scope.errorCity = false;
                        $scope.errorPropPincode = false;
                        $scope.errorCountry = false;
                        $scope.errorState = false;
                        $scope.errorPropertyPrice = false;
                        $scope.errorPropertyFacing = false;
                        $scope.errorPropertyLatLong = false;
                        $scope.errorPropImages = false;
                        $scope.errorPropertyFilter = false;
    
                        $scope.errorFloorCovering = false;
                        $scope.errorInternalCondition = false;
                        $scope.errorExternalCondition = false;
    
                        $scope.errorPropertyAge = false;
                        $scope.errorNoOfParking = false;
                        $scope.errorNoOfBedRooms = false;
    
                        $scope.errorNoOfBathRooms = false;
                        $scope.errorNoOfStudyRooms = false;
                        $scope.errorNoOfReceptionRooms = false;
    
                        $scope.errorCellarBasement = false;
                        $scope.errorCableSatellite = false;
                        $scope.errorBlindsCurtains = false;
    
                        swal("Success!", "Property Added Successfully!", "success");
                    } else {
                        console.log(response.records);
                    }
                });
            }
        }
    }

    function loadStates(ContryCode) {
        var req = {
            method: 'GET',
            url: domainAddress+"CityState/getState.php?countryID="+ContryCode
        }
        $http(req).then(function(resp) {
            $scope.listStates = resp.data.records;
        });
    }

    function loadCities(stateCode) {
        var req = {
            method: 'GET',
            url: domainAddress+"CityState/getCity.php?stateID="+stateCode
        }
        $http(req).then(function(resp) {
            $scope.listCities = resp.data.records;
        });
    }

    $scope.removeImage = function(id) {
        $("#propImage-"+id).parent().remove();

        var req = {
            method: 'POST',
            url: domainAddress+"DeletePropertyImage/"+id
        }
        $http(req).then(function(resp) {
            if(resp.data.status == 'success') {
                console.log("Success");
            }
        });
    }

    function alldaysInitialCheck() {
        $scope.isAllDays = true;
        $scope.isMonday = true;
        $scope.isTuesday = true;
        $scope.isWednesday = true;
        $scope.isThursday = true;
        $scope.isFriday = true;
        $scope.isSaturday = true;
        $scope.isSunday = true;

        $scope.mondayCheckedFalse = false;
        $scope.tuesdayCheckedFalse = false;
        $scope.wednesdayCheckedFalse = false;
        $scope.thursdayCheckedFalse = false;
        $scope.fridayCheckedFalse = false;
        $scope.saturdayCheckedFalse = false;
        $scope.sundayCheckedFalse = false;
    }

    $scope.onAllDaysChecked = function() {
        if($scope.isAllDays) {
            $scope.isMonday = true;
            $scope.isTuesday = true;
            $scope.isWednesday = true;
            $scope.isThursday = true;
            $scope.isFriday = true;
            $scope.isSaturday = true;
            $scope.isSunday = true;
        } else {
            $scope.isMonday = false;
            $scope.isTuesday = false;
            $scope.isWednesday = false;
            $scope.isThursday = false;
            $scope.isFriday = false;
            $scope.isSaturday = false;
            $scope.isSunday = false;
        }

        $scope.onDaysChecked(); 
    }

    $scope.onDaysChecked = function() {
        if($scope.isMonday) {
            $scope.mondayCheckedFalse = false;
            $scope.errorOpeningHours = false;
        } else {
            $scope.mondayCheckedFalse = true;
        }
        if($scope.isTuesday) {
            $scope.tuesdayCheckedFalse = false;
            $scope.errorOpeningHours = false;
        } else {
            $scope.tuesdayCheckedFalse = true;
        }
        if($scope.isWednesday) {
            $scope.wednesdayCheckedFalse = false;
            $scope.errorOpeningHours = false;
        } else {
            $scope.wednesdayCheckedFalse = true;
        }
        if($scope.isThursday) {
            $scope.thursdayCheckedFalse = false;
            $scope.errorOpeningHours = false;
        } else {
            $scope.thursdayCheckedFalse = true;
        }
        if($scope.isFriday) {
            $scope.fridayCheckedFalse = false;
            $scope.errorOpeningHours = false;
        } else {
            $scope.fridayCheckedFalse = true;
        }
        if($scope.isSaturday) {
            $scope.saturdayCheckedFalse = false;
            $scope.errorOpeningHours = false;
        } else {
            $scope.saturdayCheckedFalse = true;
        }
        if($scope.isSunday) {
            $scope.sundayCheckedFalse = false;
            $scope.errorOpeningHours = false;
        } else {
            $scope.sundayCheckedFalse = true;
        }

        if($scope.isMonday && $scope.isTuesday && $scope.isWednesday && $scope.isThursday && $scope.isFriday && $scope.isSaturday && $scope.isSunday) {
            $scope.isAllDays = true;
        } else {
            $scope.isAllDays = false;
            $scope.errorOpeningHours = true;
        }
    }
});

app.controller('FileUploadCtrl', function($scope) {
    //============== DRAG & DROP =============
    // source for drag&drop: http://www.webappers.com/2011/09/28/drag-drop-file-upload-with-html5-javascript/
    $scope.userID = '';

    var getZingyMoveUserData = JSON.parse(localStorage.getItem("zingyMoveUserInfo"));
    if(getZingyMoveUserData!=null){
        $scope.userID = getZingyMoveUserData[0].userID;
    }

    var dropbox = document.getElementById("dropbox")
    $scope.dropText = 'Drop Property Images here...'

    // init event handlers
    function dragEnterLeave(evt) {
        evt.stopPropagation()
        evt.preventDefault()
        $scope.$apply(function(){
            $scope.dropText = 'Drop Property Images here...'
            $scope.dropClass = ''
        })
    }
    dropbox.addEventListener("dragenter", dragEnterLeave, false)
    dropbox.addEventListener("dragleave", dragEnterLeave, false)
    dropbox.addEventListener("dragover", function(evt) {
        evt.stopPropagation()
        evt.preventDefault()
        var clazz = 'not-available'
        var ok = evt.dataTransfer && evt.dataTransfer.types && evt.dataTransfer.types.indexOf('Files') >= 0
        $scope.$apply(function(){
            $scope.dropText = ok ? 'Drop Property Images here...' : 'Only files are allowed!'
            $scope.dropClass = ok ? 'over' : 'not-available'
        })
    }, false)
    dropbox.addEventListener("drop", function(evt) {
        console.log('drop evt:', JSON.parse(JSON.stringify(evt.dataTransfer)))
        evt.stopPropagation()
        evt.preventDefault()
        $scope.$apply(function(){
            $scope.dropText = 'Drop Property Images here...'
            $scope.dropClass = ''
        })
        var files = evt.dataTransfer.files
        if (files.length > 0) {
            $scope.$apply(function(){
                $scope.files = []
                for (var i = 0; i < files.length; i++) {
                    $scope.files.push(files[i])
                }
            })
        }

        setTimeout(() => {
            $scope.uploadFile();
        },300);
    }, false)
    //============== DRAG & DROP =============

    $scope.setFiles = function(element) {
        $scope.$apply(function($scope) {
            console.log('files:', element.files);
            // Turn the FileList object into an Array
                $scope.files = []
                for (var i = 0; i < element.files.length; i++) {
                $scope.files.push(element.files[i])
                }
            $scope.progressVisible = false;
            $("#uploadProgress").hide();
        });

        setTimeout(() => {
            $scope.uploadFile();
        },300);
    };

    $scope.uploadFile = function() {
        var fd = new FormData()
        for (var i in $scope.files) {
            fd.append("uploadedFile[]", $scope.files[i])
        }
        fd.append("userID",$scope.userID)
        // console.log($scope.files[0]);
        var xhr = new XMLHttpRequest()
        xhr.upload.addEventListener("progress", uploadProgress, false)
        xhr.addEventListener("load", uploadComplete, false)
        xhr.addEventListener("error", uploadFailed, false)
        xhr.addEventListener("abort", uploadCanceled, false)
        xhr.open("POST", domainAddress+"UploadPropertyImage")
        $scope.progressVisible = true;
        $("#uploadProgress").show();
        xhr.send(fd)
    }

    function uploadProgress(evt) {
        $scope.$apply(function(){
            if (evt.lengthComputable) {
                $scope.progress = Math.round(evt.loaded * 100 / evt.total)
            } else {
                $scope.progress = 'unable to compute'
            }
        })
    }

    function uploadComplete(evt) {
        var resp = JSON.parse(evt.target.responseText);
        console.log(resp.files+" -- "+resp.propImageIds);
        $("#propImageIds").val(resp.propImageIds);


        if(resp.propImageIds == '' || resp.propImageIds == null) {
            $scope.errorPropImages = true;
        } else {
            $scope.errorPropImages = false;
        }

        $("#propImageSrc").val(resp.files);
        var propImageIDArr = resp.propImageIds.split(',');
        for($i = 0; $i < resp.files.length; $i++) {
            $("#upload_Preview").append('<div class="imageContainer"><div class="fileRemove" id="propImage-'+propImageIDArr[$i]+'" onclick="angular.element(this).scope().removeImage('+propImageIDArr[$i]+')">x</div><img src='+domainAddress+resp.files[$i]+' class="img-responsive" /></div>');
        }
        $scope.progressVisible = false;
        $("#uploadProgress").hide();
    }

    function getRandomSize(min, max) {
        return Math.round(Math.random() * (max - min) + min);
    }

    function uploadFailed(evt) {
        alert("There was an error attempting to upload the file.")
    }

    function uploadCanceled(evt) {
        $scope.$apply(function(){
            $scope.progressVisible = false;
            $("#uploadProgress").hide();
        })
        alert("The upload has been canceled by the user or the browser dropped the connection.")
    }
});