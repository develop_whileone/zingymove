app.controller('approveUserController', function($routeParams, $timeout, $http, $rootScope, $window, $scope, $sce) {
   $scope.userID = $routeParams.ID;
   $scope.userName = $routeParams.name;
   
   $scope.approvedSuccess = false;

   $scope.approveUser = function() {
    var req = {
        method: 'POST',
        url: domainAddress+'ApproveUser/'+$scope.userID,
    }
    $http(req).then(function(resp) {
        
        var response = resp.data;
        if(response.status === "success"){
            var records = response.records;
            swal({
                title: "Success!",
                text: "Approval Succeed!",
                type: "success"
            }, function() {
                window.location = "/";
            });
            $scope.approvedSuccess = true;
        } else {
            swal("Oops!", "Something went wrong!", "error");
            $scope.approvedSuccess = false;
        }
    });
   }
});