app.controller('landingController', function($timeout, $http, $rootScope, $window, $scope, $sce, $compile) {

    $(".select2").select2();

    $scope.openDetails = function (propertyID) {
        openModal(propertyID);
    }
    $(".map-wrapper").attr( "style", "height:"+(screen.availHeight - 120)+"px !important");
    var optimizedDatabaseLoading = 0;
    var _latitude = 51.507351;
    var _longitude = -0.127758;
    var element = "map-homepage";
    var markerTarget = "modal"; // use "sidebar", "infobox" or "modal" - defines the action after click on marker
    var sidebarResultTarget = "modal"; // use "sidebar", "modal" or "new_page" - defines the action after click on marker
    var showMarkerLabels = false; // next to every marker will be a bubble with title
    var mapDefaultZoom = 8; // default zoom
    heroMap(_latitude,_longitude, element, markerTarget, sidebarResultTarget, showMarkerLabels, mapDefaultZoom);


        var automaticGeoLocation = false;

        var lastClickedMarker;
        var searchClicked;
        var mapAutoZoom;
        var map;

// Hero Map on Landing ----------------------------------------------------------------------------------------------------

        function heroMap(_latitude,_longitude, element, markerTarget, sidebarResultTarget, showMarkerLabels, mapDefaultZoom){
            if( document.getElementById(element) != null ){
                // Create google map first -------------------------------------------------------------------------------------
                map = new google.maps.Map(document.getElementById(element), {
                    zoom: mapDefaultZoom,
                    scrollwheel: false,
                    center: new google.maps.LatLng(_latitude, _longitude),
                    mapTypeId: "roadmap",
                    styles: [{"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"color":"#c6c6c6"}]},{"featureType":"landscape","elementType":"all","stylers":[{"color":"#f2f2f2"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":-100},{"lightness":45}]},{"featureType":"road.highway","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"all","stylers":[{"color":"#dde6e8"},{"visibility":"on"}]}]
                });
            }

            google.maps.event.addListenerOnce(map, 'idle', function(){
                var req = {
                    method: 'GET',
                    url: domainAddress+'GetAllPropertyList',
                }
                $http(req).then(function(resp) {
                    
                    var response = resp.data;
                    if(response.status === "success"){
                        var records = response.records;
                        $('.results-number').html(records.length);
                        for(var i = 0; i < records.length; i++) {
                            if(records[i].propertyImages != null && records[i].propertyImages != undefined) {
                                for(var j = 0; j < records[i].propertyImages.length; j++) {
                                    records[i].propertyImages[j] = domainAddress+records[i].propertyImages[j];
                                }
                            }
                        }
                        $scope.resultAllProperties = records;
                        placeMarkers(records);

                        $scope.carousalArr = [];
                        for(var i = 0; i < (records.length - 1); i++) {
                            $scope.carousalArr[i] = records[i];
                        }
                    } else {
                        console.log(response);
                    }
                });
            });
           
        }
        function placeMarkers(markers){

            newMarkers = [];
            for (i = 0; i < markers.length; i++) {

                var marker;
                var markerContent = document.createElement('div');
                var thumbnailImage;

                if( markers[i].propertyImages != undefined && markers[i].propertyImages != null){
                    thumbnailImage = markers[i]["propertyImages"][0];
                }
                else {
                    thumbnailImage = "assets/common/locationUI/img/items/default.png";
                }

                if( markers[i]["featured"] == 1 ){
                    markerContent.innerHTML =
                    '<div class="marker" data-id="'+ markers[i]["propertyID"] +'">' +
                        '<div class="title">'+ markers[i]["propertyTitle"] +'</div>' +
                        '<div class="marker-wrapper">' +
                            '<div class="tag"><i class="fa fa-check"></i></div>' +
                            '<div class="pin">' +
                                '<div class="image" style="background-image: url('+ thumbnailImage +');"></div>' +
                            '</div>' +
                        '</div>' +
                    '</div>';
                }
                else {
                    markerContent.innerHTML =
                        '<div class="marker" data-id="'+ markers[i]["propertyID"] +'" >' +
                            '<div class="title">'+ markers[i]["propertyTitle"] +'</div>' +
                            '<div class="marker-wrapper">' +
                                '<div class="pin">' +
                                '<div class="image" style="background-image: url('+ thumbnailImage +');"></div>' +
                            '</div>' +
                        '</div>';
                }

                // Latitude, Longitude and Address                
                renderRichMarker(i);
            }
            function reloadMap(){
                google.maps.event.trigger(map, 'resize');
            }

            // Create marker using RichMarker plugin -------------------------------------------------------------------

            function renderRichMarker(i){
                marker = new RichMarker({
                    position: new google.maps.LatLng( markers[i]["propertyLat"], markers[i]["propertyLong"] ),
                    map: map,
                    draggable: false,
                    content: markerContent,
                    flat: true
                });

                google.maps.event.addListener(marker, 'click', (function(marker, i) {
                    return function() {
                        openModal($(this.content.firstChild).attr("data-id"), "modal_item.php");
                    }
                })(marker, i));
                newMarkers.push(marker);
            }

            // Marker clusters -----------------------------------------------------------------------------------------

            var clusterStyles = [
                {
                    url: 'assets/common/locationUI/img/cluster.png',
                    height: 36,
                    width: 36
                }
            ];

            markerCluster = new MarkerClusterer(map, newMarkers, { styles: clusterStyles, maxZoom: 16, ignoreHidden: true });

            // Show results in sidebar after map is moved --------------------------------------------------------------

            google.maps.event.addListener(map, 'idle', function() {
                renderResults();
            });

            renderResults();
            function renderResults(){               
                resultsArray = [];
                visibleMarkersId = [];
                visibleMarkersOnMap = [];
                visibleMarkerArr = [];
                for (var i = 0; i < newMarkers.length; i++) {
                    if ( map.getBounds().contains(newMarkers[i].getPosition()) ){
                        visibleMarkersOnMap.push(newMarkers[i]);
                        visibleMarkersId.push( $(newMarkers[i].content.firstChild).attr("data-id") );
                        newMarkers[i].setVisible(true);
                    }
                    else {
                        newMarkers[i].setVisible(false);
                    }
                }
                markerCluster.repaint();

                for(var i = 0; i < $scope.resultAllProperties.length; i++) {
                    for(var j = 0; j < visibleMarkersId.length; j++) {
                        
                        if(visibleMarkersId[j] == $scope.resultAllProperties[i].propertyID) {
                            visibleMarkerArr.push($scope.resultAllProperties[i]);
                        }
                    }
                }
                renderSideBarResults(visibleMarkerArr);
            }
            function renderSideBarResults(data) {
                var sideBarContent = "";
                for(var i = 0; i < data.length; i++) {
                    var PropImages = '';
                    if(data[i].propertyImages != undefined && data[i].propertyImages != null) {
                        PropImages = data[i].propertyImages[0];
                    } else {
                        PropImages = "assets/common/locationUI/img/items/default.png";
                    }

                    sideBarContent += '<div class="item" data-id="'+data[i].propertyID+'"><figure class="ribbon">&#163; &nbsp; '+data[i].propertyPrice+'</figure><a href="javascript:(0)" ng-click="openDetails('+data[i].propertyID+')"><div class="description"><div class="label label-default">'+data[i].propertyType+'</div><h3>'+data[i].propertyTitle+'</h3><h4>'+data[i].propertyAddress+'</h4></div><div class="image" style="background-image: url('+PropImages+')"></div></a><div class="additional-info"><div class="controls-more"><ul><li><a href="javascript:(0)" ng-click="saveProperty('+data[i].propertyID+')">Add to Favourites</a></li></ul></div></div></div>';   
                }
                var $el = $(sideBarContent);
                $(".results-number").html(data.length);  
                $compile($el)($scope);              
                $(".results-content").html($el);
            }
        }
        
        function openModal(target){
            $("body").append('<div class="modal modal-external fade" id="'+ target +'" tabindex="-1" role="dialog" aria-labelledby="'+ target +'"><i class="loading-icon fa fa-circle-o-notch fa-spin"></i></div>');

            $("#" + target + ".modal").on("show.bs.modal", function () {
                var _this = $(this);
                lastModal = _this;
                // alert(domainAddress + modalPath);
                var req = {
                    method: 'POST',
                    url: domainAddress+'GetPropertyDetails/'+target,
                }
                $http(req).then(function(resp) {
                    var records = resp.data.records[0];

                    var propFeaturesTags = '<ul class="tags">';
                            if(records.isFrontGarden === '1') {
                                propFeaturesTags += '<li>Front Garden</li>';
                            }
                            if(records.isRearGarden === '1') {
                                propFeaturesTags += '<li>Rear Garden</li>';
                            }
                            if(records.isExtractorFanBathroom === '1') {
                                propFeaturesTags += '<li>Extractor Fan Bathroom</li>';
                            }
                            if(records.isSeperateWC === '1') {
                                propFeaturesTags += '<li>Seperate Water Closet</li>';
                            }
                            if(records.isEnsuite === '1') {
                                propFeaturesTags += '<li>Ensuite</li>';
                            }
                            if(records.utilityRoom === '1') {
                                propFeaturesTags += '<li>Utility Room</li>';
                            }
                            if(records.heatingGas === '1') {
                                propFeaturesTags += '<li>Heating Gas</li>';
                            }
                            if(records.heatingElectricty === '1') {
                                propFeaturesTags += '<li>Heating Electricty</li>';
                            }
                            if(records.smokeAlarm === '1') {
                                propFeaturesTags += '<li>Smoke Alarm</li>';
                            }
                            if(records.monoxideAlarm === '1') {
                                propFeaturesTags += '<li>Monoxide Alarm</li>';
                            }
                            if(records.chimney === '1') {
                                propFeaturesTags += '<li>Chimney</li>';
                            }
                            if(records.saniflo === '1') {
                                propFeaturesTags += '<li>Saniflo</li>';
                            }
                            if(records.doubleGlazing === '1') {
                                propFeaturesTags += '<li>Double Glazing</li>';
                            }
                            if(records.isKitchenWashingMachin === '1') {
                                propFeaturesTags += '<li>Kitchen Washing Machine</li>';
                            }
                            if(records.isKitchenTumbleDryer === '1') {
                                propFeaturesTags += '<li>Kitchen Tumble Dryer</li>';
                            }
                            if(records.isKitchenOven === '1') {
                                propFeaturesTags += '<li>Kitchen Oven</li>';
                            }
                            if(records.isKitchenHob === '1') {
                                propFeaturesTags += '<li>Kitchen Hob</li>';
                            }
                            if(records.isExtractorFan === '1') {
                                propFeaturesTags += '<li>Extractor Fan</li>';
                            }
                    propFeaturesTags += '</ul>';

                    var propertyImages='<div class="gallery owl-carousel" data-owl-nav="1" data-owl-dots="0" style="margin-left: 5px">';
                    if(records.propertyImages != undefined && records.propertyImages != null) {
                        for(image in records.propertyImages) {
                            propertyImages += '<img src="'+domainAddress+records.propertyImages[image]+'" alt="">';
                        }
                    } else {
                        propertyImages += '<img src="assets/common/locationUI/img/items/no_photo.jpg" alt="">'; 
                    }
                    
                    propertyImages +='</div>';
                    
                    var availableHours='<div class="col-md-12 col-sm-12"><h3>Available Hours</h3><div class="table-responsive"><table class="table borderless"><thead><tr><th>Days</th><th>Morning</th><th>Evening</th></tr></thead><tbody>';

                    if(records.isModay == '1') {
                        availableHours+='<tr><td>Monday</td>';
                        if((records.mondayAMFrom != '' && records.mondayAMFrom != undefined)|| (records.mondayAMTill != '' && records.mondayAMTill != undefined)) {
                            // alert(1);
                            availableHours+='<td>'+records.mondayAMFrom+'-'+records.mondayAMTill+'</td>';
                        } else {
                            availableHours+='<td>-</td>';
                        }
                        if((records.mondayPMFrom != '' && records.mondayPMFrom != undefined)|| (records.mondayPMTill != '' && records.mondayPMTill != undefined)) {
                            // alert(1);
                            availableHours+='<td>'+records.mondayPMFrom+'-'+records.mondayPMTill+'</td>';
                        } else {
                            availableHours+='<td>-</td>';
                        }
                        availableHours+='</tr>';
                    }
                    if(records.isTuesday == '1') {
                        availableHours+='<tr><td>Tuesday</td>';
                        if((records.tuesdayAMFrom != '' && records.tuesdayAMFrom != undefined)|| (records.tuesdayAMTill != '' && records.tuesdayAMTill != undefined)) {
                            // alert(1);
                            availableHours+='<td>'+records.tuesdayAMFrom+'-'+records.tuesdayAMTill+'</td>';
                        } else {
                            availableHours+='<td>-</td>';
                        }
                        if((records.tuesdayPMFrom != '' && records.tuesdayPMFrom != undefined)|| (records.tuesdayPMTill != '' && records.tuesdayPMTill != undefined)) {
                            // alert(1);
                            availableHours+='<td>'+records.tuesdayPMFrom+'-'+records.tuesdayPMTill+'</td>';
                        } else {
                            availableHours+='<td>-</td>';
                        }
                        availableHours+='</tr>';
                    }
                    if(records.isWednesday == '1') {
                        availableHours+='<tr><td>Wednesday</td>';
                        if((records.wednesdayAMFrom != '' && records.wednesdayAMFrom != undefined)|| (records.wednesdayAMTill != '' && records.wednesdayAMTill != undefined)) {
                            // alert(1);
                            availableHours+='<td>'+records.wednesdayAMFrom+'-'+records.wednesdayAMTill+'</td>';
                        } else {
                            availableHours+='<td>-</td>';
                        }
                        if((records.wednesdayPMFrom != '' && records.wednesdayPMFrom != undefined)|| (records.wednesdayPMTill != '' && records.wednesdayPMTill != undefined)) {
                            // alert(1);
                            availableHours+='<td>'+records.wednesdayPMFrom+'-'+records.wednesdayPMTill+'</td>';
                        } else {
                            availableHours+='<td>-</td>';
                        }
                        availableHours+='</tr>';
                    }
                    if(records.isThursday == '1') {
                        availableHours+='<tr><td>Thursday</td>';
                        if((records.thursdayAMFrom != '' && records.thursdayAMFrom != undefined)|| (records.thursdayAMTill != '' && records.thursdayAMTill != undefined)) {
                            // alert(1);
                            availableHours+='<td>'+records.thursdayAMFrom+'-'+records.thursdayAMTill+'</td>';
                        } else {
                            availableHours+='<td>-</td>';
                        }
                        if((records.thursdayPMFrom != '' && records.thursdayPMFrom != undefined)|| (records.thursdayPMTill != '' && records.thursdayPMTill != undefined)) {
                            // alert(1);
                            availableHours+='<td>'+records.thursdayPMFrom+'-'+records.thursdayPMTill+'</td>';
                        } else {
                            availableHours+='<td>-</td>';
                        }
                        availableHours+='</tr>';
                    }
                    if(records.isFriday == '1') {
                        availableHours+='<tr><td>Friday</td>';
                        if((records.fridayAMFrom != '' && records.fridayAMFrom != undefined)|| (records.fridayAMTill != '' && records.fridayAMTill != undefined)) {
                            // alert(1);
                            availableHours+='<td>'+records.fridayAMFrom+'-'+records.fridayAMTill+'</td>';
                        } else {
                            availableHours+='<td>-</td>';
                        }
                        if((records.fridayPMFrom != '' && records.fridayPMFrom != undefined)|| (records.fridayPMTill != '' && records.fridayPMTill != undefined)) {
                            // alert(1);
                            availableHours+='<td>'+records.fridayPMFrom+'-'+records.fridayPMTill+'</td>';
                        } else {
                            availableHours+='<td>-</td>';
                        }
                        availableHours+='</tr>';
                    }
                    if(records.isSaturday == '1') {
                        availableHours+='<tr><td>Saturday</td>';
                        if((records.saturdayAMFrom != '' && records.saturdayAMFrom != undefined)|| (records.saturdayAMTill != '' && records.saturdayAMTill != undefined)) {
                            // alert(1);
                            availableHours+='<td>'+records.saturdayAMFrom+'-'+records.saturdayAMTill+'</td>';
                        } else {
                            availableHours+='<td>-</td>';
                        }
                        if((records.saturdayPMFrom != '' && records.saturdayPMFrom != undefined)|| (records.saturdayPMTill != '' && records.saturdayPMTill != undefined)) {
                            // alert(1);
                            availableHours+='<td>'+records.saturdayPMFrom+'-'+records.saturdayPMTill+'</td>';
                        } else {
                            availableHours+='<td>-</td>';
                        }
                        availableHours+='</tr>';
                    }
                    if(records.isSunday == '1') {
                        availableHours+='<tr><td>Sunday</td>';
                        if((records.sundayAMFrom != '' && records.sundayAMFrom != undefined)|| (records.sundayAMTill != '' && records.sundayAMTill != undefined)) {
                            // alert(1);
                            availableHours+='<td>'+records.sundayAMFrom+'-'+records.sundayAMTill+'</td>';
                        } else {
                            availableHours+='<td>-</td>';
                        }
                        if((records.sundayPMFrom != '' && records.sundayPMFrom != undefined)|| (records.sundayPMTill != '' && records.sundayPMTill != undefined)) {
                            // alert(1);
                            availableHours+='<td>'+records.sundayPMFrom+'-'+records.sundayPMTill+'</td>';
                        } else {
                            availableHours+='<td>-</td>';
                        }
                        availableHours+='</tr>';
                    }

                    availableHours += '</tbody></table></div></section>';

                    var results = '<div class="modal-item-detail modal-dialog propDetailsModal" role="document" data-latitude="'+records.propertyLat+'" data-longitude="'+records.propertyLong+'" data-address="'+records.propertyAddress+'"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button><div class="section-title"><h2>'+records.propertyTitle+'</h2><div class="label label-default">Flats</div><div class="controls-more"><ul><li><a href="#">Add to favorites</a></li><li><a href="#">Add to watchlist</a></li></ul></div></div></div><div class="modal-body"><div class="left">'+propertyImages+'<div class="map" id="map-modal"></div><section><h3>Contact</h3><h5><i class="fa fa-map-marker"></i>'+records.propertyAddress+'</h5><h5><i class="fa fa-building"></i>'+records.propertyCity+'</h5><h5><i class="fa fa-flag"></i>'+records.propertyCounty+'</h5></section></div><div class="right" style="width: 50%"><section><h3>Description</h3><div class="read-more" style="word-break: break-all"><p>'+records.propertyDesc+'</p></div></section><section><h3>Features</h3><p>'+records.propertyFeatures+'</p><br>'+propFeaturesTags+'</section></div>'+availableHours+'</div><div class="modal-footer" style="align-items: center; justify-content: center"><button type="button" class="btn btn-success" ng-click="contactOwner('+records.propertyID+')">Contact Owner</button></div></div></div>';


                    
                    var $element = $(results);
                    $compile($element)($scope);
                    
                    _this.append($element);
                    $('head').append( $('<link rel="stylesheet" type="text/css">').attr('href', 'assets/common/locationUI/css/bootstrap-select.min.css') );
                    $(".selectpicker").selectpicker();

                    if( $("input[type=file]").length ){
                        $.getScript( "assets/common/locationUI/js/jQuery.MultiFile.min.js", function( data, textStatus, jqxhr ) {
                            $("input.file-upload-input").MultiFile({
                                list: ".file-upload-previews"
                            });
                        });
                    }

                    _this.find(".gallery").addClass("owl-carousel");
                    ratingPassive(".modal");
                    var img = _this.find(".gallery img:first")[0];
                    if( img ){
                        $(img).load(function() {
                            timeOutActions(_this);
                        });
                    }
                    else {
                        timeOutActions(_this);
                    }
                    socialShare();
                    _this.on("hidden.bs.modal", function () {
                        $(lastClickedMarker).removeClass("active");
                        $(".pac-container").remove();
                        _this.remove();
                    });
                });

                var req = {
                    method: 'POST',
                    url: domainAddress+"UpdatePropertyView/"+target
                }
                $http(req).then(function(resp) {
                    if(resp.data.status == 'success') {
                        console.log("Success");
                    }
                });
            });

            $("#" + target + ".modal").modal("show");

            function timeOutActions(_this){
                setTimeout(function(){
                    if( _this.find(".map").length ){
                        if( _this.find(".modal-dialog").attr("data-address") ){
                            simpleMap( 0, 0, "map-modal", _this.find(".modal-dialog").attr("data-marker-drag"), _this.find(".modal-dialog").attr("data-address") );
                        }
                        else {
                            simpleMap( _this.find(".modal-dialog").attr("data-latitude"), _this.find(".modal-dialog").attr("data-longitude"), "map-modal", _this.find(".modal-dialog").attr("data-marker-drag") );
                        }
                    }
                    $("#map-modal").attr("style","margin-left: 5px");
                    initializeOwl();
                    initializeFitVids();
                    initializeReadMore();
                    _this.addClass("show");
                }, 200);

            }

        }
        if( $('.ui-slider').length > 0 ){
            $('.ui-slider').each(function() {
                if( $("body").hasClass("rtl") ) var rtl = "rtl";
                else rtl = "ltr";

                var step;
                if( $(this).attr('data-step') ) {
                    step = parseInt( $(this).attr('data-step') );
                }
                else {
                    step = 1000;
                }
                var sliderElement = $(this).attr('id');
                var element = $( '#' + sliderElement);
                var valueMin = parseInt( $(this).attr('data-value-min') );
                var valueMax = parseInt( $(this).attr('data-value-max') );
                $(this).noUiSlider({
                    start: [ valueMin, valueMax ],
                    connect: true,
                    direction: rtl,
                    range: {
                        'min': valueMin,
                        'max': valueMax
                    },
                    step: step
                });
                if( $(this).attr('data-value-type') == 'price' ) {
                    if( $(this).attr('data-currency-placement') == 'before' ) {
                        $(this).Link('lower').to( $(this).children('.values').children('.value-min'), null, wNumb({ prefix: $(this).attr('data-currency'), decimals: 0, thousand: '.' }));
                        $(this).Link('upper').to( $(this).children('.values').children('.value-max'), null, wNumb({ prefix: $(this).attr('data-currency'), decimals: 0, thousand: '.' }));
                    }
                    else if( $(this).attr('data-currency-placement') == 'after' ){
                        $(this).Link('lower').to( $(this).children('.values').children('.value-min'), null, wNumb({ postfix: $(this).attr('data-currency'), decimals: 0, thousand: ' ' }));
                        $(this).Link('upper').to( $(this).children('.values').children('.value-max'), null, wNumb({ postfix: $(this).attr('data-currency'), decimals: 0, thousand: ' ' }));
                    }
                }
                else {
                    $(this).Link('lower').to( $(this).children('.values').children('.value-min'), null, wNumb({ decimals: 0 }));
                    $(this).Link('upper').to( $(this).children('.values').children('.value-max'), null, wNumb({ decimals: 0 }));
                }
            });
        }
        
    
    $scope.isSubmitted = false;
    $scope.userTitle = 'Mr.';
    $scope.userType = 'Owner';
    
    $scope.onPhoneChange = function(mobileNumber) {
        if(mobileNumber == '') {
            $scope.errorMobileNumberRequired = true;
        } else if(isNaN(mobileNumber)) {
            $scope.errorMobileNumber = true;
        } else {
            $scope.errorMobileNumber = false;
            $scope.errorMobileNumberRequired = false;
        }
    }
    $scope.onPasswordChange = function(password) {
        if(password.length > 5) {
            $scope.errorPasswordLength = false;
        } else {
            $scope.errorPasswordLength = true;
        }
    }
    $scope.onConfirmPassChange = function(confirm_password) {
        if(confirm_password.length > 5 && (confirm_password == $scope.password)) {
            $scope.errorConfirmPass = false;
        } else {
            $scope.errorConfirmPass = true;
        }
    }
    $scope.onRegister= function() {
        $scope.isSubmitted = true;
        if($scope.mobileNumber == '' || $scope.mobileNumber == undefined) {
            $scope.errorMobileNumberRequired = true;
        } else if(isNaN($scope.mobileNumber)) {
            $scope.errorMobileNumber = true;
        } else if($scope.password.length < 6) {
            $scope.errorPasswordLength = true;
        } else if($scope.password != $scope.confirm_password) {
            $scope.errorConfirmPass = true;
        } else {  
            var postdata = '{"userTitle":"'+$scope.userTitle+'","userName":"'+$scope.userName+'","userEmail": "'+$scope.email+'","userPhone": "'+$scope.mobileNumber+'","userPassword": "'+$scope.password+'","userType": "'+$scope.userType+'"}';
            
            console.log(postdata);
            var req = {
                method: 'POST',
                url: domainAddress+'CreateUser',
                data: postdata
            }
            $http(req).then(function(resp) {
                $scope.spinnerload='';
                $scope.alreadyExist = false;
                var response = resp.data;
                if(response.status === "Success"){
                    $('#modal-register').modal('toggle');
                    localStorage.setItem("zingyMoveUserInfo", JSON.stringify(response.records));
                        
                    var getZingyMoveLoginData = response.records;
                    console.log(JSON.stringify(getZingyMoveLoginData));
                    $rootScope.$emit("UserAccessControlMethod", {});
                    if(getZingyMoveLoginData[0].userType == 'Owner'){
                        $rootScope.$emit("CallTopMenuMethod", {});
                        window.location.href="#!/DashboardOwner";
                        window.location.reload();
                    }
                    else if(getZingyMoveLoginData[0].userType == 'Renter'){
                        $rootScope.$emit("CallTopMenuMethod", {});
                        window.location.href="#!/DashboardRenter";
                        window.location.reload();
                    } else {
                        $rootScope.$emit("CallTopMenuMethod", {});
                        window.location.href="#!/DashboardRenter";
                        window.location.reload();
                    }
                } else if(response.status === 'Exists') {
                    swal("Oops!", "User Already Exists!", "warning");
                } else {
                    console.log(response.status);
                }
            });
        }
    }

    $scope.registerFromLoginModal = function() {
        $("#modal-sign-in").modal("toggle");
        setTimeout(() => {
            $("#modal-register").modal("toggle");
        },500);
    }

    $scope.onLoginMailChange = function(loginMail) {
        $scope.loginFailed = false;
        if($scope.loginForm.loginMail.$valid) {
            $scope.errorLoginMail = false;
        }
    }
    
    $scope.onLoginPassChange = function(loginPass) {
        $scope.loginFailed = false;
        if(loginPass == '') {
            $scope.errorLoginPass = true;
        } else {
            $scope.errorLoginPass = false;
        }
    }

    $scope.onLogin = function() {
        $scope.isSubmitted = true;
        if($scope.loginForm.loginMail.$invalid) {
            $scope.errorLoginMail = true;
        } else if($scope.loginPass == '' || $scope.loginPass == undefined) {
            $scope.errorLoginPass = true;
        } else {
            var postdata = '{"userPhone":"","userPassword":"'+$scope.loginPass+'","userEmail":"'+$scope.loginMail+'"}';
            
            console.log(postdata);
            var req = {
                method: 'POST',
                url: domainAddress+'LoginUser',
                data: postdata
            }
            $http(req).then(function(resp) {
                var response = resp.data;
                if(response.status === "success"){
                    $('#modal-sign-in').modal('toggle');
                    $('.modal-backdrop').removeClass('modal-backdrop');
                    localStorage.setItem("zingyMoveUserInfo", JSON.stringify(response.records));
                        
                    var getZingyMoveLoginData = response.records;
                    console.log(JSON.stringify(getZingyMoveLoginData));
                    
                    $rootScope.$emit("UserAccessControlMethod", {});
                    if(getZingyMoveLoginData[0].userType == 'Owner'){
                        $rootScope.$emit("CallTopMenuMethod", {});
                        window.location.href="#!/DashboardOwner";
                        window.location.reload();
                    } else if(getZingyMoveLoginData[0].userType == 'Renter'){
                        if($scope.isContactOwner) {
                            $rootScope.$emit("CallTopMenuMethod", {});
                            window.location.href="#!/PropertyDetails?propID="+$scope.contactPropID+"&type=contactOwner";
                            window.location.reload();
                        } else if($scope.isPropertySave) {
                            $rootScope.$emit("CallTopMenuMethod", {});
                            window.location.href="#!/PropertyDetails?propID="+$scope.savePropID+"&type=saveProperty";
                            window.location.reload();
                        } else {
                            $rootScope.$emit("CallTopMenuMethod", {});
                            window.location.href="#!/DashboardRenter";
                            window.location.reload();
                        }
                    } else if(getZingyMoveLoginData[0].userType == 'Super Admin'){
                        $rootScope.$emit("CallTopMenuMethod", {});
                        window.location.href="#!/SuperAdminDashboard";
                        window.location.reload();
                    } else if(getZingyMoveLoginData[0].userType == 'Admin'){
                        $rootScope.$emit("CallTopMenuMethod", {});
                        window.location.href="#!/AdminDashboard";
                        window.location.reload();
                    } else {
                        $rootScope.$emit("CallTopMenuMethod", {});
                        window.location.href="#!/DashboardRenter";
                        window.location.reload();
                    }
                } else if(response.status === "failure"){
                    $scope.loginFailed = true;
                    $scope.loginMail = '';
                    $scope.loginPass = '';
                    $scope.errorLoginMail = false;
                    $scope.errorLoginPass = false;
                    // $('#modal-sign-in').modal('toggle');
                } else {
                    console.log("Error in  login");
                }
            });
        }
    }
    
    $scope.searchProperties = function() {
        var minPrice = $("#priceMinValue").val().replace(/\D/g,'');
        var maxPrice = $("#priceMaxValue").val().replace(/\D/g,'');

        var propertyCity = $scope.propertyCity;
        var propertyType = $scope.propertyType;

        // console.log(minPrice+" - "+maxPrice+" - "+city+" - "+propertyType);
        
        var postData = '{ "minPrice":"'+minPrice+'","maxPrice":"'+maxPrice+'","propertyCity":"'+propertyCity+'","propertyType":"'+propertyType+'" }';
        
        var req = {
            method: 'POST',
            url: domainAddress+'SearchProperties',
            data: postData
        }
        $http(req).then(function(resp) {
            if(resp.data.status === "success"){
                var records = resp.data.records;
                $('.results-number').html(resp.data.record_count);
                for(var i = 0; i < records.length; i++) {
                    for(var j = 0; j < records[i].propertyImages.length; j++) {
                        records[i].propertyImages[j] = domainAddress+records[i].propertyImages[j];
                    }
                }
                console.log(records);
                // renderSideBarResults(records);

                var sideBarContent = "";
                for(var i = 0; i < records.length; i++) {
                    sideBarContent += '<div class="item" data-id="'+records[i].propertyID+'"><figure class="ribbon">&#163; &nbsp'+records[i].propertyPrice+'</figure><a href="javascript:(0)" ng-click="openDetails('+records[i].propertyID+')"><div class="description"><div class="label label-default">'+records[i].propertyType+'</div><h3>'+records[i].propertyTitle+'</h3><h4>'+records[i].propertyAddress+'</h4></div><div class="image" style="background-image: url('+records[i].propertyImages[0]+')"></div></a><div class="additional-info"><div class="controls-more"><ul><li><a href="#">Add to favorites</a></li><li><a href="#">Add to watchlist</a></li><li><a href="#" class="quick-detail">Quick detail</a></li></ul></div></div></div>';   
                }
                var $el = $(sideBarContent);
                // $(".results-number").html(records.length);
                $compile($el)($scope);
                $(".results-content").html($el);


            } else if(resp.data.status === "failure"){
                
            } else {
                console.log("Error in  Search");
            }
        });
    }
    $scope.isContactOwner = false;
    $scope.isPropertySave = false;
    $scope.contactPropID = '';
    $scope.savePropID = '';
    $('#modal-sign-in').on('hidden.bs.modal', function () {
        $scope.isContactOwner = false;
        $scope.isPropertySave = false;
    });
    $('#modal-register').on('hidden.bs.modal', function () {
        $scope.isContactOwner = false;
        $scope.isPropertySave = false;
    });
    
    $scope.contactOwner = function(propID) {
        $scope.isContactOwner = true;
        $scope.contactPropID = propID;
        var getZingyMoveUserData = JSON.parse(localStorage.getItem("zingyMoveUserInfo"));
        if(getZingyMoveUserData!=null) {
            // alert("logged in");
        } else {
            // $(".propDetailsModal").modal('toggle');
            $("#modal-sign-in").modal('toggle');
        }
    }
    $scope.saveProperty = function(propID) {
        $scope.isPropertySave = true;
        $scope.savePropID = propID;
        var getZingyMoveUserData = JSON.parse(localStorage.getItem("zingyMoveUserInfo"));
        if(getZingyMoveUserData!=null) {
            // alert("logged in");
        } else {
            $("#modal-sign-in").modal('toggle');
        }
    }

        var getZingyMoveUserData = JSON.parse(localStorage.getItem("zingyMoveUserInfo"));
        if(getZingyMoveUserData!=null){

            $rootScope.$emit("UserAccessControlMethod", {});
            if(getZingyMoveUserData[0].userType == 'Owner'){
                $rootScope.$emit("CallTopMenuMethod", {});
                window.location.href="#!/DashboardOwner";
                window.location.reload();
            }
            else if(getZingyMoveUserData[0].userType == 'Renter'){
                $rootScope.$emit("CallTopMenuMethod", {});
                window.location.href="#!/DashboardRenter";
                window.location.reload();
            } else if(getZingyMoveUserData[0].userType == 'Super Admin'){
                $rootScope.$emit("CallTopMenuMethod", {});
                window.location.href="#!/SuperAdminDashboard";
                window.location.reload();
            } else if(getZingyMoveUserData[0].userType == 'Admin'){
                $rootScope.$emit("CallTopMenuMethod", {});
                window.location.href="#!/AdminDashboard";
                window.location.reload();
            } else {
                $rootScope.$emit("CallTopMenuMethod", {});
                window.location.href="#!/DashboardRenter";
                window.location.reload();
            }
        } 
});