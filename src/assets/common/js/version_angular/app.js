'use strict';

// var domainAddress = "http://localhost:8888/zingymoveapi/";
var domainAddress = "http://api.zingymove.co.uk/";

angular.module('cleanUI', [
    'ngRoute',
    'cleanUI.controllers',
])
.config(['$locationProvider', '$routeProvider',
    function($locationProvider, $routeProvider) {

        /////////////////////////////////////////////////////////////
        // SYSTEM
        $routeProvider.when('/', {redirectTo: '/Landing'});
        $routeProvider.otherwise({redirectTo: 'pages/page-404'});

        $routeProvider.when('/SuperAdminDashboard', {
            templateUrl: 'dashboards/superAdminDashboard.html',
            controller: 'superAdminDashboardController'
        });

        $routeProvider.when('/AdminDashboard', {
            templateUrl: 'dashboards/adminDashboard.html',
            controller: 'adminDashboardController'
        });

        $routeProvider.when('/DashboardRenter', {
            templateUrl: 'dashboards/dashboardRenter.html',
            controller: 'renterDashboardController'
        });
        
        $routeProvider.when('/DashboardOwner', {
            templateUrl: 'dashboards/dashboardOwner.html',
            controller: 'ownerDashboardController'
        });

        $routeProvider.when('/Profile', {
            templateUrl: 'pages/profile.html',
            controller: 'profileController'
        });

        $routeProvider.when('/UserDetails', {
            templateUrl: 'pages/userDetails.html',
            controller: 'userDetailsController'
        });

        $routeProvider.when('/Landing', {
            templateUrl: 'dashboards/landing.html',
            controller: 'landingController'
        });

        $routeProvider.when('/AddProperty', {
            templateUrl: 'pages/addProperty.html',
            controller: 'addPropertyController'
        });

        $routeProvider.when('/EditProperty', {
            templateUrl: 'pages/editProperty.html',
            controller: 'editPropertyController'
        });

        $routeProvider.when('/ListProperty', {
            templateUrl: 'pages/listProperty.html',
            controller: 'listPropertyController'
        });

        $routeProvider.when('/PropertyDetails', {
            templateUrl: 'pages/propertyDetails.html',
            controller: 'propertyDetailsController'
        });

        $routeProvider.when('/HowItWorks', {
            templateUrl: 'pages/howItWorks.html',
            controller: 'introController'
        });

        $routeProvider.when('/ApproveUser', {
            templateUrl: 'pages/approveUser.html',
            controller: 'approveUserController'
        });

        $routeProvider.when('/AllProperties', {
            templateUrl: 'pages/allProperties.html',
            controller: 'allPropertiesController'
        });

        $routeProvider.when('/pages/page-404', {
            templateUrl: 'pages/page-404.html'
        });

        $routeProvider.when('/pages/page-500', {
            templateUrl: 'pages/page-500.html'
        });

       
    }
]);

var app = angular.module('cleanUI.controllers', []);

app.controller('MainCtrl', function($location, $scope, $rootScope, $timeout, $http) {

    NProgress.configure({
        minimum: 0.2,
        trickleRate: 0.1,
        trickleSpeed: 200
    });

    $scope.$on('$routeChangeStart', function() {

        // NProgress Start
        $('body').addClass('cui-page-loading-state');
        NProgress.start();

    });

    $scope.$on('$routeChangeSuccess', function() {

        // Set to default (show) state left and top menu, remove single page classes
        $('body').removeClass('single-page single-page-inverse');
        $rootScope.hideLeftMenu = false;
        $rootScope.hideTopMenu = false;
        $rootScope.showFooter = true;

        // Firefox issue: scroll top when page load
        $('html, body').scrollTop(0);

        // Set active state menu after success change route
        $('.left-menu-list-active').removeClass('left-menu-list-active');
        $('nav.left-menu .left-menu-list-root .left-menu-link').each(function(){
            if ($(this).attr('href') == '#' + $location.path()) {
                $(this).closest('.left-menu-list-root > li').addClass('left-menu-list-active');
            }
        });

        // NProgress End
        setTimeout(function(){
            NProgress.done();
        }, 1000);
        $('body').removeClass('cui-page-loading-state');
    });
});

